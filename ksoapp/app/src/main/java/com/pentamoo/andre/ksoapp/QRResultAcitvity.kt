package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_event.*
import kotlinx.android.synthetic.main.activity_qrresult_acitvity.*
import org.jetbrains.anko.contentView
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class QRResultAcitvity : AppCompatActivity() {
    var qrcode = ""
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> super.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qrresult_acitvity)

        setSupportActionBar(toolbar3)
        supportActionBar?.title="Scan Result"

        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        qrcode = intent.getStringExtra("hasilqr")

        btnScanQR.setOnClickListener {
            val intent = Intent(this, QRActivity::class.java)
            startActivity(intent)
        }
       // txtInviteeName.text = qrcode

        btnUpdate.setOnClickListener {
            doAsync {
                val db =
                    Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
                var invDao = db.inviteeDao()
                var inviteeData:ArrayList<InviteeModel> = invDao.getInviteeByQR(qrcode) as ArrayList<InviteeModel>

                var attendancesDao = db.attendancesDao()
                attendancesDao.updateId(txtNumPerson.text.toString().toInt(), txtAngpau.text.toString().toInt(), txtDesc.text.toString(), inviteeData[0].id)

                uiThread {
                    contentView?.snackbar("Invitee Updated")
                }
            }

        }


        doAsync {
            var x = 1
            val db =
                Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
            var invDao = db.inviteeDao()
            var inviteeData:ArrayList<InviteeModel> = invDao.getInviteeByQR(qrcode) as ArrayList<InviteeModel>
            var eventDao = db.eventDao()
            var inviteeDao = db.inviteeDao()

            // cek qr code valid atau tidak
            if(inviteeData.size > 0) {
                txtInvalidQR.visibility = View.INVISIBLE
                txtInviteeName.text = inviteeData[0].title + " " + inviteeData[0].name
                txtQR.text = inviteeData[0].qr_code

                if(inviteeData[0].side == "L") {
                    txtSide.text = "Side: Groom"
                } else if(inviteeData[0].side == "P") {
                    txtSide.text = "Side: Bride"
                } else {
                    txtSide.visibility = View.INVISIBLE
                }

                if(inviteeData[0].namatable != "") {
                    txtTable.text = "Table: " + inviteeData[0].namatable
                } else {
                    txtTable.visibility = View.INVISIBLE
                }

                // cek dan simpan invitee
                var attendancesDao = db.attendancesDao()
                var cek = attendancesDao.getId(inviteeData[0].id)
                if(cek.size > 0 ) {
                    // load
                    var invdata = attendancesDao.getId(inviteeData[0].id)
                    // update UI
                    txtNumPerson.setText(invdata[0].total_pax.toString())
                    txtAngpau.setText(invdata[0].total_angpau.toString())
                    txtDesc.setText(invdata[0].description.toString())
                } else {
                    //insert
                    val sharedPrefs = applicationContext.getSharedPreferences(
                        "USERNAME", Context.MODE_PRIVATE
                    )
                    var username = sharedPrefs.getString("USERNAME", null)

                    var event = eventDao.getId(inviteeData[0].event_id!!)
                    var eventid = event[0].id
                    var tot:ArrayList<AttendancesModel> = attendancesDao.getTotalAttend(eventid, username) as ArrayList<AttendancesModel>
                    var code = event[0].initial + (tot.size+ 1)





                    var inviteeId = inviteeData[0].id

                    attendancesDao.insert(AttendancesModel(0, "", code, "", "", "", "", "scan","",1, 1,username,inviteeId,eventid, false))
                    inviteeDao.updateAttending(1, inviteeId)
                }
                 contentView?.snackbar("Invitee Updated")
            } else {
                txtInviteeName.visibility = View.INVISIBLE
                txtQR.visibility = View.INVISIBLE
                txtSide.visibility = View.INVISIBLE
                txtTable.visibility =View.INVISIBLE
            }


            uiThread {
                // longToast(x.toString())
                //window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                //progressBar.visibility = View.INVISIBLE
            }
        }
    }
}
