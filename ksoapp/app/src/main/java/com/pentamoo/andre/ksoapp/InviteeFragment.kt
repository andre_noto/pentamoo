package com.pentamoo.andre.ksoapp


import android.annotation.SuppressLint
import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.uiThread


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class InviteeFragment : Fragment() {
    companion object {
        var eventid:Int = 0
        var filter = "Show All"
        var viewV:View ?= null

        fun newInstance(): InviteeFragment {
            return InviteeFragment()
        }
    }

    override fun onResume() {
        super.onResume()
        var inviteeList:ArrayList<InviteeModel> = ArrayList()
        doAsync {
            //  var x = 1
            val db =
                Room.databaseBuilder(activity!!.applicationContext, AppDatabase::class.java, Utility.dbname).build()
            var inviteeDao = db.inviteeDao()
            val sharedPrefs = context!!.getSharedPreferences(
                "CURRENT_EVENT_ID", Context.MODE_PRIVATE
            )
            eventid = sharedPrefs.getString("CURRENT_EVENT_ID", null).toInt()

            if(filter == "Attend") {
                inviteeList= inviteeDao.showAllAttend(eventid, "") as ArrayList<InviteeModel>
            } else if(filter == "Not Attend") {
                inviteeList= inviteeDao.showNotAttend(eventid) as ArrayList<InviteeModel>
            } else {
                inviteeList = inviteeDao.searchInvitee(eventid, "") as ArrayList<InviteeModel>
            }

            uiThread {
                //supportActionBar?.title=eventlist[0].nama.toString()
                var adapter = ArrayAdapter<InviteeModel>(activity!!.applicationContext, android.R.layout.simple_list_item_1, inviteeList)
                var lst = viewV?.findViewById<ListView>(R.id.listinvitee)
                lst?.adapter = null

                lst?.adapter = adapter

                lst?.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                    //Log.d("ceknama", adapter.getItem(i).name)
                    val intent = Intent(context, ManualActivity::class.java)
                    intent.putExtra("inviteedid", adapter.getItem(i).id)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    context?.startActivity(intent)
                }
            }
        }
    }

    @SuppressLint("RestrictedApi")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var v =  inflater.inflate(R.layout.fragment_invitee, container, false)
        var tt = v.findViewById<EditText>(R.id.txtSearch)
        var sa = v.findViewById<TextView>(R.id.txtViewFilter)
        viewV = v

        var fab = v.findViewById<FloatingActionButton>(R.id.floatingActionButton)
        fab.setOnClickListener {
            val intent = Intent(context, NewInviteeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context?.startActivity(intent)
        }

        if(HomeActivity.user_roles == "registrator") {
            fab.visibility = View.VISIBLE
        } else {
            fab.visibility = View.INVISIBLE
        }

        sa.setOnClickListener {
           tt.setText("")

            if(filter == "Show All") {
                filter = "Attend"
            } else if(filter == "Attend") {
                filter = "Not Attend"
            } else {
                filter = "Show All"
            }

            sa.setText(filter)
            var inviteeList:ArrayList<InviteeModel> = ArrayList()
            doAsync {
              //  var x = 1
                val db =
                    Room.databaseBuilder(activity!!.applicationContext, AppDatabase::class.java, Utility.dbname).build()
                var inviteeDao = db.inviteeDao()
                val sharedPrefs = context!!.getSharedPreferences(
                    "CURRENT_EVENT_ID", Context.MODE_PRIVATE
                )
                eventid = sharedPrefs.getString("CURRENT_EVENT_ID", null).toInt()

                if(filter == "Attend") {
                    inviteeList= inviteeDao.showAllAttend(eventid) as ArrayList<InviteeModel>
                } else if(filter == "Not Attend") {
                    inviteeList= inviteeDao.showNotAttend(eventid) as ArrayList<InviteeModel>
                } else {
                    inviteeList = inviteeDao.searchInvitee(eventid, "") as ArrayList<InviteeModel>
                }

                uiThread {
                    //supportActionBar?.title=eventlist[0].nama.toString()
                    var adapter = ArrayAdapter<InviteeModel>(activity!!.applicationContext, android.R.layout.simple_list_item_1, inviteeList)
                    var lst = v.findViewById<ListView>(R.id.listinvitee)
                    lst.adapter = null

                    lst.adapter = adapter

                    lst.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                        //Log.d("ceknama", adapter.getItem(i).name)
                        val intent = Intent(context, ManualActivity::class.java)
                        intent.putExtra("inviteedid", adapter.getItem(i).id)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        context?.startActivity(intent)
                    }
                }
            }
        }



        tt.addTextChangedListener(object:TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(tt.text.toString() != "") {
                    var inviteeList: ArrayList<InviteeModel>
                    doAsync {
                        var x = 1
                        val db =
                            Room.databaseBuilder(activity!!.applicationContext, AppDatabase::class.java, Utility.dbname)
                                .build()
                        var inviteeDao = db.inviteeDao()
                        val sharedPrefs = context!!.getSharedPreferences(
                            "CURRENT_EVENT_ID", Context.MODE_PRIVATE
                        )
                        eventid = sharedPrefs.getString("CURRENT_EVENT_ID", null).toInt()


                        if(filter == "Attend") {
                            //, tt.text.toString()
                            inviteeList= inviteeDao.showAllAttend(eventid, tt.text.toString()) as ArrayList<InviteeModel>
                        } else if(filter == "Not Attend") {
                            inviteeList= inviteeDao.showNotAttend(eventid, tt.text.toString()) as ArrayList<InviteeModel>
                        } else {
                            inviteeList = inviteeDao.searchInvitee(eventid, tt.text.toString()) as ArrayList<InviteeModel>
                        }

                        uiThread {
                            //supportActionBar?.title=eventlist[0].nama.toString()
                            Log.d("cariattend", filter + " " + tt.text.toString())
                            var adapter = ArrayAdapter<InviteeModel>(
                                activity!!.applicationContext,
                                android.R.layout.simple_list_item_1,
                                inviteeList
                            )
                            var lst = v.findViewById<ListView>(R.id.listinvitee)
                            lst.adapter = adapter
                        }
                    }
                }

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        doAsync {
            var x = 1
            val db =
                Room.databaseBuilder(activity!!.applicationContext, AppDatabase::class.java, Utility.dbname).build()
            var inviteeDao = db.inviteeDao()
            val sharedPrefs = context!!.getSharedPreferences(
                "CURRENT_EVENT_ID", Context.MODE_PRIVATE
            )
            eventid = sharedPrefs.getString("CURRENT_EVENT_ID", null).toInt()
            var inviteeList:ArrayList<InviteeModel> = inviteeDao.getInvitee(eventid) as ArrayList<InviteeModel>

            uiThread {
                //supportActionBar?.title=eventlist[0].nama.toString()
                var adapter = ArrayAdapter<InviteeModel>(activity!!.applicationContext, android.R.layout.simple_list_item_1, inviteeList)
                var lst = v.findViewById<ListView>(R.id.listinvitee)
                lst.adapter = adapter


                lst.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                    //Log.d("ceknama", adapter.getItem(i).name)
                    val intent = Intent(context, ManualActivity::class.java)
                    intent.putExtra("inviteedid", adapter.getItem(i).id)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    context?.startActivity(intent)
                }
               // activity?.longToast("oke")
            }
        }


        return v
    }


}
