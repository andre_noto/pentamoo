package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.Room
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_new_invitee.*
import org.jetbrains.anko.contentView
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.uiThread

class NewInviteeActivity : AppCompatActivity() {

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> super.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_invitee)
        setSupportActionBar(toolbar5)


        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        supportActionBar?.title="New Invitee"

        // populate title dan side
        var tit = arrayOf("Mr.", "Mrs.", "Ms.")
        var sd = arrayOf("Laki-laki", "Perempuan")
        var adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tit)
        var adapter2 = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sd)
        spinTitle.adapter = adapter
        spinSide.adapter = adapter2

        var invitee = ArrayList<String>()
        val sharedPrefs = applicationContext.getSharedPreferences(
            "CURRENT_EVENT_ID", Context.MODE_PRIVATE
        )
        var eid = sharedPrefs.getString("CURRENT_EVENT_ID", null)


        doAsync {
            val db =
                Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
            var inv = db.inviteeDao()
            invitee = inv.getTableName(eid.toInt()) as ArrayList<String>

            //ambil table

            uiThread {
                var adapter3 = ArrayAdapter<String>(applicationContext, android.R.layout.simple_list_item_1, invitee)
                txtTableName.setAdapter(adapter3)
                txtTableName.threshold = 1
                longToast("set")
            }
        }

        btnSave.setOnClickListener {

            // nama ga boleh kosong
            if(txtInviteeName.text.toString() == "") {
                contentView?.snackbar("Invitee name cant be empty")
            } else {
                // insert ke db
                doAsync {

                    val db =
                        Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
                    var inv = db.inviteeDao()

                    var side  = "L"
                    if(spinSide.selectedItem.toString() == "Perempuan") {
                        side = "P"
                    }

                    var im:InviteeModel = InviteeModel(0, txtInviteeName.text.toString(), "", txtAddress.text.toString(), txtInviteeEmail.text.toString(), txtDesc.text.toString(), 0, "", 1, txtPhone.text.toString(), side, "pending", "", spinTitle.selectedItem.toString(), eid.toInt(),txtTableName.text.toString(), ""  )
                    inv.insert(im)
                    uiThread {
                        txtTableName.setText("")
                        txtInviteeName.setText("")
                        txtAddress.setText("")
                        spinTitle.setSelection(0)
                        txtInviteeEmail.setText("")
                        txtPhone.setText("")
                        spinSide.setSelection(0)
                        txtDesc.setText("")

                        contentView?.snackbar("New invitee added successfully")
                    }
                }
            }
        }


    }
}
