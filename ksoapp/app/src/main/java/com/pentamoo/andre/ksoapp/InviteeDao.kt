package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.*

@Dao
interface InviteeDao {
    @Query("SELECT * from invitee")
    fun getAll(): List<InviteeModel>

    @Query("SELECT * FROM invitee WHERE event_id =:eventId ORDER BY name")
    fun getInvitee(eventId:Int): List<InviteeModel>


    @Query("SELECT * FROM invitee WHERE event_id =:eventId AND code='' ORDER BY id DESC")
    fun getRecentAddedInvitee(eventId:Int): List<InviteeModel>


    @Query("SELECT * FROM invitee WHERE event_id =:eventId AND name  LIKE '%' || :name || '%' ")
    fun searchInvitee(eventId: Int, name:String): List<InviteeModel>


    @Query("SELECT * FROM invitee WHERE qr_code =:qrcode ORDER BY name")
    fun getInviteeByQR(qrcode:String): List<InviteeModel>


    @Query("SELECT DISTINCT namatable FROM invitee WHERE event_id =:eventId ORDER BY namatable ASC")
    fun getTableName(eventId:Int): List<String>
//AND name  LIKE '%' || :name || '%'
    @Query("SELECT * FROM invitee WHERE is_attending = 1 AND event_id =:eventId AND name LIKE '%' || :name || '%'   ORDER BY name")
    fun showAllAttend(eventId: Int, name:String = ""): List<InviteeModel>

    @Query("SELECT * FROM invitee WHERE is_attending = 0 AND event_id =:eventId AND name  LIKE '%' || :name || '%'  ORDER BY name")
    fun showNotAttend(eventId: Int,  name:String = ""): List<InviteeModel>

    @Query("SELECT * FROM invitee WHERE id =:invid ")
    fun getInviteeById(invid:Int): List<InviteeModel>

    @Query("UPDATE invitee SET is_attending =:isattending WHERE id =:invid")
    fun updateAttending(isattending:Int, invid:Int)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(im: InviteeModel)

    @Delete
    fun delete(im: InviteeModel)

    @Query("DELETE FROM invitee")
    fun deleteAll()

}
