package com.pentamoo.andre.ksoapp


import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class DashboardFragment : Fragment() {
    companion object {
        var sudahisilist = false
        var ehm:ArrayList<EventHasUserModel> ?= null
        var context: Context?= null
        fun newInstance(): DashboardFragment {
            return DashboardFragment()
        }
    }

    var v:View ?= null
    var tat:TextView ?= null
    var tgs:TextView ?= null
    var tot:TextView?=null
    var pb:ProgressBar?=null
    var tdt:TextView ?= null
    var lst:ListView ?= null

    var totalattendance:ArrayList<Int> = ArrayList()
    var inv:ArrayList<InviteeModel> ?= null
    var tv9:TextView ?= null

    fun populateRecent() {
        //context?.toast("tes her")
        var totInvitee = "0"
        var totAtt = "0"
        var tgl = ""

        if(OptionsFragment.gateOpen == true) {
            tgs?.text = "GATE IS OPEN"
            tgs?.setTextColor(ResourcesCompat.getColor(resources, R.color.colorHijau, null))
        } else {
            tgs?.text = "GATE IS CLOSE"
            tgs?.setTextColor(ResourcesCompat.getColor(resources, R.color.colorMerah, null))
        }



        if(HomeActivity.user_roles == "registrator") {
            tgs?.visibility = View.INVISIBLE
            tv9?.text = "Recently Added"
        } else if(HomeActivity.user_roles == "event_manager") {
            tgs?.visibility = View.INVISIBLE
            tv9?.text = "Receptionists"
        } else {
            tgs?.visibility = View.VISIBLE
            tv9?.text = "Recently Scanned"
        }


        if(HomeActivity.user_roles =="event_manager") {
            view?.findViewById<TextView>(R.id.txtLoginAs)?.text = "event manager"
        } else {
            view?.findViewById<TextView>(R.id.txtLoginAs)?.text = HomeActivity.user_roles
        }

        var tes:ArrayList<AttendancesModel> = ArrayList<AttendancesModel>()

        doAsync {
            try{
                val db =
                    Room.databaseBuilder(context!!, AppDatabase::class.java, Utility.dbname).build()
                var inviteeDao = db.inviteeDao()
                var attendancesDao = db.attendancesDao()
                var eventDao = db.eventDao()

                var eventHasUserDao = db.eventHasUserDao()



                val sharedPrefs = context!!.getSharedPreferences(
                    "CURRENT_EVENT_ID", Context.MODE_PRIVATE
                )
                var cureventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)

                var event = eventDao.getId(cureventid.toInt())
                var inv2:ArrayList<InviteeModel> = inviteeDao.getInvitee(cureventid.toInt()) as ArrayList<InviteeModel>
                totInvitee =  inv2.size.toString()

                tgl = event[0].tanggal_mulai.toString()

                var unique:ArrayList<String> = ArrayList<String>()

                tes = attendancesDao.getAllSpecificEvent(cureventid.toInt()) as ArrayList<AttendancesModel>

                tes.forEach {
                        it ->
                    var x: List<String> =   unique.filter { t -> t == it.invitee_id.toString() }
                    if(x.size == 0) {
                        unique.add(it.invitee_id.toString())
                    }
                }
                totAtt = unique.size.toString()

                if(HomeActivity.user_roles == "event_manager") {

                    ehm = eventHasUserDao.getIdWithRoles(cureventid.toInt(), "receptionist") as ArrayList<EventHasUserModel>

                    ehm?.forEach {

                        var t = attendancesDao.getTotalAttend(cureventid.toInt(), it.user_username.toString())
                        totalattendance.add(t.size)
                    }
                }

                if(HomeActivity.user_roles == "registrator") {
                    inv = inviteeDao.getRecentAddedInvitee(cureventid.toInt()) as ArrayList<InviteeModel>
                } else {
                    // get recent scan
                    val sharedPrefs = activity!!.applicationContext.getSharedPreferences(
                        "USERNAME", Context.MODE_PRIVATE
                    )
                    var username = sharedPrefs.getString("USERNAME", null)

                    var recent = attendancesDao.getAllRecent(cureventid.toInt(), username  )
                    inv = ArrayList<InviteeModel>()
                    inv?.clear()

                    recent.forEach { it ->
                        var c = inviteeDao.getInviteeById(it.invitee_id!!)
                        var ada = false
                        inv?.forEach { x ->
                            if(x.id == c[0].id) {
                                ada = true
                            }
                        }
                        if(ada == false) {
                            inv!!.add(c[0])
                        }
                    }
                }
            } catch(e:Exception) {
               Log.d("exceptiondashboard", e.message.toString())
            }

            uiThread {
               // context?.toast("vent" + invdata.size)

                Log.d("populaterecent", "enter")
                tat?.text = totAtt + "/" + totInvitee

                pb?.max = totInvitee.toInt()
                pb?.progress = totAtt.toInt()
                Log.d("tanggal", tgl)
                tdt?.text = Utility.convertDate(tgl)

                lst?.adapter = null


                Log.d("updatepopulate", "true")

                if(HomeActivity.user_roles == "event_manager") {

                    DashboardFragment.ehm?.forEach {
                        Log.d("cekog", it.toString())
                    }

                    var adapter = EventCardAdapter(DashboardFragment.context!!, DashboardFragment.ehm!!, totalattendance)
                    lst?.adapter = adapter

                    lst?.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                        //Log.d("ceknama", adapter.getItem(i).name)
                        val intent = Intent(context, ReceptionistActivity::class.java)
                        intent.putExtra("username", adapter.getItem(i).user_username)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        context?.startActivity(intent)
                    }

                } else {
                    inv?.forEach {
                        Log.d("cekbug", it.toString())
                    }

                    inv?.distinct()
                    var adapter = ArrayAdapter<InviteeModel>(
                        activity!!.applicationContext,
                        android.R.layout.simple_list_item_1,
                        inv
                    )
                //lst = v?.findViewById<ListView>(R.id.recentScan)

                    if(adapter.isEmpty == false) {
                        lst?.adapter = adapter

                        lst?.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                            //Log.d("ceknama", adapter.getItem(i).name)
                            val intent = Intent(context, ManualActivity::class.java)
                            intent.putExtra("inviteedid", adapter.getItem(i).id)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            context?.startActivity(intent)
                        }
                    }

                }
            }
        }
        Log.d("cekonstart", "started")
    }

    override fun onStart() {
        super.onStart()
       // populateRecent()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_dashboard, container, false)
        tv9 = v?.findViewById<TextView> (R.id.textView9)

        tgs = v?.findViewById<TextView>(R.id.txtGateStatus)
       if(OptionsFragment.gateOpen == true) {
            tgs?.text = "GATE IS OPEN"
            tgs?.setTextColor(ResourcesCompat.getColor(resources, R.color.colorHijau, null))
        } else {
            tgs?.text = "GATE IS CLOSE"
            tgs?.setTextColor(ResourcesCompat.getColor(resources, R.color.colorMerah, null))
        }

        if(HomeActivity.user_roles == "event_manager") {
            tgs?.visibility = View.INVISIBLE
        }


        if(HomeActivity.user_roles == "registrator") {
            tgs?.visibility = View.INVISIBLE
            tv9?.text = "Recently Added"
        } else {
            tgs?.visibility = View.VISIBLE
            tv9?.text = "Recently Scanned"
        }


        // context?.toast("cek here create view")
        var totInvitee = "0"
        var totAtt = "0"
        var tgl = ""
        var tes:ArrayList<AttendancesModel> = ArrayList()

        view?.findViewById<TextView>(R.id.txtLoginAs)?.text =  HomeActivity.user_roles


       doAsync {
            try {
                val db =
                    Room.databaseBuilder(context!!, AppDatabase::class.java, Utility.dbname).build()
                var inviteeDao = db.inviteeDao()
                var attendancesDao = db.attendancesDao()
                var eventDao = db.eventDao()



                val sharedPrefs = context!!.getSharedPreferences(
                    "CURRENT_EVENT_ID", Context.MODE_PRIVATE
                )
                var cureventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)

                var event = eventDao.getId(cureventid.toInt())

                var inv2: ArrayList<InviteeModel> =
                    inviteeDao.getInvitee(cureventid.toInt()) as ArrayList<InviteeModel>
                totInvitee = inv2.size.toString()

                tgl = event[0].tanggal_mulai.toString()

                tes = attendancesDao.getAllSpecificEvent(cureventid.toInt()) as ArrayList<AttendancesModel>
                var unique:ArrayList<String> = ArrayList<String>()

                tes.forEach {
                    it ->
                      var x: List<String> =   unique.filter { t -> t == it.invitee_id.toString() }
                      if(x.size > 0) {
                        unique.add(it.invitee_id.toString())
                      }
                }
                totAtt = unique.size.toString()


                if(HomeActivity.user_roles == "registrator") {
                    inv = inviteeDao.getRecentAddedInvitee(cureventid.toInt()) as ArrayList<InviteeModel>
                } else {
                    // get recent scan
                    val sharedPrefs = activity!!.applicationContext.getSharedPreferences(
                        "USERNAME", Context.MODE_PRIVATE
                    )
                    var username = sharedPrefs.getString("USERNAME", null)

                    var recent = attendancesDao.getAllRecent(cureventid.toInt(), username  )
                    inv = ArrayList<InviteeModel>()
                    inv?.clear()

                    recent.forEach { it ->
                        var c = inviteeDao.getInviteeById(it.invitee_id!!)
                        var ada = false
                        inv?.forEach { x ->
                            if(x.id == c[0].id) {
                                ada = true
                            }
                        }
                        if(ada == false) {
                            inv!!.add(c[0])
                        }
                    }       }

            } catch(e:Exception) {
                Log.d("exceptiondashboard", e.message.toString())
            }


            uiThread {
                tat = v?.findViewById<TextView>(R.id.txtAttendances)
                //tot  = v?.findViewById<TextView>(R.id.txtTotalInvitee)
                pb = v?.findViewById<ProgressBar>(R.id.progressAttendance)
                tdt = v?.findViewById<TextView>(R.id.txtEventDate)


                //Log.d("checkinvitee", Utility.CURRENT_EVENT_ID.toString())


                tat?.text = totAtt + "/" + totInvitee
              //  tot?.text = totInvitee

                val sharedPrefs = context!!.getSharedPreferences(
                    "CURRENT_EVENT_ID", Context.MODE_PRIVATE
                )
                var cureventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)

                Log.d("cureventcek", totAtt)

                pb?.max = totInvitee.toInt()
                pb?.progress = totAtt.toInt()

               // tdt?.text = Utility.convertDate(tgl)


                // jika registrator tampilkan recent added


                inv?.distinct()
                var adapter = ArrayAdapter<InviteeModel>(activity!!.applicationContext, android.R.layout.simple_list_item_1, inv)
                lst = v?.findViewById<ListView>(R.id.recentScan)
                //lst?.adapter = null

                lst?.adapter = adapter
                Log.d("updateoncreate", "true")


                lst?.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                    //Log.d("ceknama", adapter.getItem(i).name)
                    val intent = Intent(context, ManualActivity::class.java)
                    intent.putExtra("inviteedid", adapter.getItem(i).id)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    context?.startActivity(intent)
                }
                sudahisilist = true

            }
        }
        return v
    }


}
