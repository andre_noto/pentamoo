package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "invitee")
data class InviteeModel (
    @PrimaryKey
    var id: Int,
    @ColumnInfo(name = "name") var name: String?,
    @ColumnInfo(name = "code") var code: String?,
    @ColumnInfo(name = "address") var address: String?,
    @ColumnInfo(name = "email") var email: String?,
    @ColumnInfo(name = "description") var description: String?,
    @ColumnInfo(name = "is_attending") var is_attending: Int?,
    @ColumnInfo(name = "message") var message: String?,
    @ColumnInfo(name = "num_of_persons") var num_of_persons: Int?,
    @ColumnInfo(name = "phone") var phone: String?,
    @ColumnInfo(name = "side") var side: String?,
    @ColumnInfo(name = "status") var status: String?,
    @ColumnInfo(name = "tag") var tag: String?,
    @ColumnInfo(name = "title") var title: String?,
    @ColumnInfo(name = "event_id") var event_id: Int?,
    @ColumnInfo(name = "namatable") var namatable: String?,
    @ColumnInfo(name="qr_code") var qr_code: String?
) {

    override fun toString():String {
        return title.toString() + " " + name.toString()
    }
}