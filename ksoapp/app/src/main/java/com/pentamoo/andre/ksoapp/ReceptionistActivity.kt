package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.view.MenuItem
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_receptionist.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import org.jetbrains.anko.contentView
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.uiThread

class ReceptionistActivity : AppCompatActivity() {
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> super.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receptionist)

        setSupportActionBar(toolbar6)

        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#555555")))

        var i = intent.getStringExtra("username")

        btnCloseOpenGate2.setOnClickListener {
            doAsync {
                val sharedPrefs = applicationContext.getSharedPreferences(
                    "CURRENT_EVENT_ID", Context.MODE_PRIVATE
                )
                var cureventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)
                val db =
                    Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()


                var eventHasUserDao = db.eventHasUserDao()
                var u = eventHasUserDao.getMyEventHasUser(i, cureventid.toInt())
                var og = u[0].open_gate
                if(og == true){
                    og = false
                } else {
                    og = true
                }

                eventHasUserDao.updateGate(og, i, cureventid.toInt())
                u = eventHasUserDao.getMyEventHasUser(i, cureventid.toInt())


                uiThread {
                    if(u[0].open_gate == true) {
                        txtGateStatus2.text = "GATE IS OPEN"
                        btnCloseOpenGate2.text = "CLOSE GATE"
                        txtGateStatus2.setTextColor(ResourcesCompat.getColor(resources, R.color.colorHijau, null))
                        contentView?.snackbar("Gate Updated. Dont forget to synch")
                    } else {
                        txtGateStatus2.text = "GATE IS CLOSE"
                        contentView?.snackbar("Gate Updated. Dont forget to synch")
                        btnCloseOpenGate2.text = "OPEN GATE"
                        txtGateStatus2.setTextColor(ResourcesCompat.getColor(resources, R.color.colorMerah, null))
                    }
                }
            }
        }

        var data:ArrayList<InviteeModel> = ArrayList()
        var attx:ArrayList<AttendancesModel> = ArrayList()

        doAsync {
            val sharedPrefs = applicationContext.getSharedPreferences(
                "CURRENT_EVENT_ID", Context.MODE_PRIVATE
            )
            var cureventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)
            val db =
                Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()


            var eventHasUserDao = db.eventHasUserDao()
            var u = eventHasUserDao.getMyEventHasUser(i, cureventid.toInt())

            if(u[0].last_sync == "null") {
                txtLastSycnhro.text = "Last sync: never"
            } else {
                txtLastSycnhro.text = "Last sync: " + u[0].last_sync
            }


            supportActionBar?.title=u[0].roles + " "  + u[0].inisial
            if(u[0].open_gate == true) {
                txtGateStatus2.text = "GATE IS OPEN"
                btnCloseOpenGate2.text = "CLOSE GATE"
                txtGateStatus2.setTextColor(ResourcesCompat.getColor(resources, R.color.colorHijau, null))
            } else {
                txtGateStatus2.text = "GATE IS CLOSE"

                btnCloseOpenGate2.text = "OPEN GATE"
                txtGateStatus2.setTextColor(ResourcesCompat.getColor(resources, R.color.colorMerah, null))
            }

            var attendancesDao = db.attendancesDao()
            var inviteeDao = db.inviteeDao()
            attx = attendancesDao.getAllByUsername(cureventid.toInt(), i) as ArrayList<AttendancesModel>

            var j =0
            attx.forEach {
                var temp = inviteeDao.getInviteeById(it.invitee_id!!.toInt())
                data.add(temp[j])
                j++
            }
            uiThread {
                var adapter = ArrayAdapter<InviteeModel>(applicationContext, android.R.layout.simple_list_item_1, data)
                lstScan.adapter = adapter

                lstScan.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                    //Log.d("ceknama", adapter.getItem(i).name)
                    val intent = Intent(applicationContext, ManualActivity::class.java)
                    intent.putExtra("inviteedid", adapter.getItem(i).id)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    applicationContext?.startActivity(intent)
                }
            }
        }
    }
}
