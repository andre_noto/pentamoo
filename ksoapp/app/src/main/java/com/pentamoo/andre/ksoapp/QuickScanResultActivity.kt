package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_qrresult_acitvity.*
import kotlinx.android.synthetic.main.activity_quick_scan.*
import kotlinx.android.synthetic.main.activity_quick_scan_result.*
import org.jetbrains.anko.contentView
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class QuickScanResultActivity : AppCompatActivity() {
    var qrcode = ""
    var jumperson = 1
    var invid = 1
    var initial = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quick_scan_result)

        qrcode = intent.getStringExtra("hasilqr")

        val sharedPrefs = applicationContext.getSharedPreferences(
            "CURRENT_EVENT_ID", Context.MODE_PRIVATE
        )
        var eventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)
        Picasso.get().load("http://jimmy.jitusolution.com/screen/" + eventid + "_blank.jpg").into(imgBg)

        txtMin.setOnClickListener {
            jumperson--
            if(jumperson < 1) {
                jumperson = 1
            }
            textView23.text = jumperson.toString()
        }

        txtPlus.setOnClickListener {
            jumperson++

            if(jumperson > 20) {
                jumperson = 20
            }
            textView23.text = jumperson.toString()

        }
        imageView12.setOnClickListener {
            this.onBackPressed()
        }

        textView25.setOnClickListener {
            this.onBackPressed()
        }

        button2.setOnClickListener {
            val intent = Intent(this, QuickScanFinishActivity::class.java)

            doAsync {
                val db =
                    Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
                var attendanceDao = db.attendancesDao()
                attendanceDao.updateTotPax(jumperson, invid)
                var inviteeDao = db.inviteeDao()
                inviteeDao.updateAttending(1,invid)
                uiThread {
                    intent.putExtra("eventid", eventid)
                    intent.putExtra("invid", invid)
                    intent.putExtra("initial", initial)
                    intent.putExtra("jumperson", jumperson)
                    //intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                    startActivity(intent)
                }
            }

        }

        var c:ArrayList<AttendancesModel> = ArrayList()
        doAsync {
            var x = 1
            val db =
                Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
            var invDao = db.inviteeDao()
            var eventDao = db.eventDao()
            var attendancesDao = db.attendancesDao()
            var inviteeDao = db.inviteeDao()
            var inviteeData:ArrayList<InviteeModel> = invDao.getInviteeByQR(qrcode) as ArrayList<InviteeModel>
            val sharedPrefs = applicationContext.getSharedPreferences(
                "USERNAME", Context.MODE_PRIVATE
            )
            var username = sharedPrefs.getString("USERNAME", null)

            c = attendancesDao.getTotalAttend(eventid.toInt(), username) as ArrayList<AttendancesModel>


            if(inviteeData.size > 0) {
                invid = inviteeData[0].id
                textView2.text = inviteeData[0].title + " " + inviteeData[0].name
                var side = ""
                if(inviteeData[0].side == "L") {
                    side = "Groom's Friends"
                } else if(inviteeData[0].side == "P") {
                    side = "Bride's Friends"
                } else {
                    textView19.visibility = View.INVISIBLE
                }

                if(inviteeData[0].namatable != "") {
                    textView19.text = inviteeData[0].namatable + " - " + side
                } else {
                    textView19.text =  side
                }

                var event = eventDao.getId(inviteeData[0].event_id!!)
                textView20.text = event[0].initial + (c.size + 1)
                initial = event[0].initial + (c.size + 1)

                var cekdata = attendancesDao.getId(invid)
                if(cekdata.size > 0 ) {
                    textView20.text = cekdata[0].code
                    textView23.text = cekdata[0].total_pax.toString()
                    jumperson = cekdata[0].total_pax!!.toInt()
                }
                /*txtInvalidQR.visibility = View.INVISIBLE
                txtInviteeName.text = inviteeData[0].title + " " + inviteeData[0].name




                txtQR.text = inviteeData[0].qr_code

                if(inviteeData[0].side == "L") {
                    txtSide.text = "Side: Groom"
                } else if(inviteeData[0].side == "P") {
                    txtSide.text = "Side: Bride"
                } else {
                    txtSide.visibility = View.INVISIBLE
                }

                if(inviteeData[0].namatable != "") {
                    txtTable.text = "Table: " + inviteeData[0].namatable
                } else {
                    txtTable.visibility = View.INVISIBLE
                }*/

                // cek dan simpan invitee
               /* var attendancesDao = db.attendancesDao()
                var cek = attendancesDao.getId(inviteeData[0].id)
                if(cek.size > 0 ) {
                    // load
                    var invdata = attendancesDao.getId(inviteeData[0].id)
                    // update UI
                    //txtNumPerson.setText(invdata[0].total_pax.toString())
                    //txtAngpau.setText(invdata[0].total_angpau.toString())
                    //txtDesc.setText(invdata[0].description.toString())
                } else {
                    //insert
                    val sharedPrefs = applicationContext.getSharedPreferences(
                        "USERNAME", Context.MODE_PRIVATE
                    )
                    var username = sharedPrefs.getString("USERNAME", null)

                    var event = eventDao.getId(inviteeData[0].event_id!!)
                    var eventid = event[0].id
                    var tot:ArrayList<AttendancesModel> = attendancesDao.getTotalAttend(eventid, username) as ArrayList<AttendancesModel>
                    var code = event[0].initial + (tot.size+ 1)
                    var inviteeId = inviteeData[0].id

                    attendancesDao.insert(AttendancesModel(0, "", code, "", "", "", "", "scan","",1, 1,username,inviteeId,eventid))
                    inviteeDao.updateAttending(1, inviteeId)
                }*/
                //contentView?.snackbar("Invitee Updated")
            } else {
                textView21.visibility = View.VISIBLE
                button2.visibility = View.INVISIBLE
                textView.visibility = View.INVISIBLE
                textView2.visibility = View.INVISIBLE
                textView19.visibility = View.INVISIBLE
                textView20.visibility = View.INVISIBLE
            }
            uiThread {
                Log.d("cekjumlah", c.size.toString() )
            }
        }
    }
}
