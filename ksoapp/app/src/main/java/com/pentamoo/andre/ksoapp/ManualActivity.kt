package com.pentamoo.andre.ksoapp

import android.annotation.SuppressLint
import android.app.VoiceInteractor
import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.drawable.ColorDrawable
import android.hardware.Camera
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.constraint.ConstraintSet
import android.support.constraint.Constraints
import android.support.media.ExifInterface
import android.support.v4.content.FileProvider
import android.support.v4.content.res.ResourcesCompat
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.TextView
import android.widget.Toast
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_manual.*
import kotlinx.android.synthetic.main.activity_quick_scan_finish.*
import org.jetbrains.anko.contentView
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class ManualActivity : AppCompatActivity() {
    var jumperson = 1
    var jumangpau = 1
    val REQUEST_TAKE_PHOTO = 1
    var currentPhotoPath  = ""
    var qrcode  = ""
    var photolink = ""
    var showInfo = false

    private val PREF_UNIQUE_ID = "PREF_UNIQUE_ID"


    var initial = ""
    private var uniqueID: String? = null


    @Synchronized
    fun id(context: Context): String {
        if (uniqueID == null) {
            val sharedPrefs = context.getSharedPreferences(
                PREF_UNIQUE_ID, Context.MODE_PRIVATE
            )
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null)

            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString()
                val editor = sharedPrefs.edit()
                editor.putString(PREF_UNIQUE_ID, uniqueID)
                editor.commit()
            }
        }

        return uniqueID as String
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        Log.d("masuksini" , "123")

        //  val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File(storageDir, "foto_${initial}.jpg"
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
            Log.d("currentphotopath_cek", currentPhotoPath)
        }

        /*  return File.createTempFile(
            "foto_${initial}", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
            Log.d("currentphotopath", currentPhotoPath)
        } */
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> super.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            val f = File(currentPhotoPath)
            var x = Uri.fromFile(f)
            Log.d("fotoskr", currentPhotoPath)
           //Toast.makeText(this, currentPhotoPath, Toast.LENGTH_LONG).show()
             var bmp: Bitmap = MediaStore.Images.Media.getBitmap(contentResolver, x)

            var ei: ExifInterface = ExifInterface(currentPhotoPath)
            var i  = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)

            var rotatedBitmap:Bitmap ?= null
            when(i) {
                ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap = rotateImage(bmp, 90f)
                ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap = rotateImage(bmp, 180f)
                ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap = rotateImage(bmp, 270f)
                ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = bmp
            }

            imageView11.setImageBitmap(rotatedBitmap)

            val file_path = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
           // val dir = File(file_path, "foto_${initial}.jpg")
            //if (!dir.exists())
              ///  dir.mkdirs()
            val file = File(file_path, "foto_${initial}.jpg")
            val fOut = FileOutputStream(file)

            rotatedBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
            fOut.flush()
            fOut.close()

            Log.d("filepath", file.absolutePath.toString())


             imageView11.setImageBitmap(rotatedBitmap)
            if(photolink != "") {
                imageView11.visibility = View.VISIBLE
            }
        }
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }

    @SuppressLint("RestrictedApi")
    fun disableAll() {
        btnAngpau1.isEnabled = false
        btnAngpau2.isEnabled = false
        btnAngpau3.isEnabled = false
        btnAngpau4.isEnabled = false
        btnAngpau5.isEnabled = false

        btnInviteePresent.visibility = View.INVISIBLE
        txtGateClosedInfo.visibility = View.INVISIBLE
        fabUpdate.visibility = View.INVISIBLE
        fabCamera.visibility = View.INVISIBLE

        btnPerson1.isEnabled = false
        btnPerson2.isEnabled = false
        btnPerson3.isEnabled = false
        btnPerson4.isEnabled = false
        btnPerson5.isEnabled = false

        txtDesc2.isEnabled = false

        btnMinAngpau.isEnabled = false
        btnMaxAngpau.isEnabled = false
        btnMinPereson.isEnabled = false
        btnPlusPerson.isEnabled = false

        txtNumPerson2.isEnabled =false
        txtNumAngpau.isEnabled = false
    }

    fun renderAngpau(nomor:Int) {
        jumangpau = nomor
        btnAngpau1.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        btnAngpau2.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        btnAngpau3.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        btnAngpau4.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        btnAngpau5.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        when(jumangpau) {
            1 -> btnAngpau1.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimaryDark, null))
            2 -> btnAngpau2.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimaryDark, null))
            3 -> btnAngpau3.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimaryDark, null))
            4 -> btnAngpau4.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimaryDark, null))
            5 -> btnAngpau5.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimaryDark, null))
        }
    }

    fun renderButton(nomor:Int){
        jumperson = nomor
        btnPerson1.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        btnPerson2.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        btnPerson3.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        btnPerson4.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        btnPerson5.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        when(jumperson) {
            1 -> btnPerson1.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimaryDark, null))
            2 -> btnPerson2.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimaryDark, null))
            3 -> btnPerson3.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimaryDark, null))
            4 -> btnPerson4.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimaryDark, null))
            5 -> btnPerson5.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimaryDark, null))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manual)



        var i = intent.getIntExtra("inviteedid", 0)


        setSupportActionBar(toolbar4)

        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        try {
            qrcode = intent.getStringExtra("hasilqr")
        } catch(ex:Exception) {

        }


        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#555555")))



        uniqueID = id(applicationContext)



        imgClose.setOnClickListener {
            imageView11.visibility = View.INVISIBLE
            imgClose.visibility = View.INVISIBLE
            imgBlack.visibility = View.INVISIBLE
            imgRetake.visibility = View.INVISIBLE
        }

        imgRetake.setOnClickListener {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                // Ensure that there's a camera activity to handle the intent
                takePictureIntent.resolveActivity(packageManager)?.also {
                    // Create the File where the photo should go
                    val photoFile: File? = try {
                        createImageFile()
                    } catch (ex: IOException) {
                        // Error occurred while creating the File

                        null
                    }
                    // Continue only if the File was successfully created
                    photoFile?.also {
                        val photoURI: Uri = FileProvider.getUriForFile(
                            this,
                            "com.pentamoo.andre.ksoapp",
                            it
                        )
                      //   takePictureIntent.putExtra("android.intent.extras.CAMERA_FACING", Camera.CameraInfo.CAMERA_FACING_FRONT)
                        takePictureIntent.putExtra("android.intent.extras.LENS_FACING_FRONT", 0)
                        takePictureIntent.putExtra("android.intent.extra.USE_BACK_CAMERA", true)
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                    }
                }
            }
        }

        fabCamera.setOnClickListener {
            if(currentPhotoPath.isBlank()) {

                Log.d("photopath2", currentPhotoPath)
                Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                    // Ensure that there's a camera activity to handle the intent
                    takePictureIntent.resolveActivity(packageManager)?.also {
                        // Create the File where the photo should go
                        val photoFile: File? = try {
                            createImageFile()
                        } catch (ex: IOException) {
                            // Error occurred while creating the File

                            null
                        }
                        // Continue only if the File was successfully created
                        photoFile?.also {
                            val photoURI: Uri = FileProvider.getUriForFile(
                                this,
                                "com.pentamoo.andre.ksoapp",
                                it
                            )
                            // takePictureIntent.putExtra("android.intent.extras.CAMERA_FACING", Camera.CameraInfo.CAMERA_FACING_FRONT)
                            //takePictureIntent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1);
                            //takePictureIntent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                        }
                    }
                }
            } else {

                try {
                    val f = File(currentPhotoPath)
                    var x = Uri.fromFile(f)
                    var bmp: Bitmap = MediaStore.Images.Media.getBitmap(contentResolver, x)
                    imageView11.setImageBitmap(bmp)
                    imageView11.visibility = View.VISIBLE
                    imgClose.visibility = View.VISIBLE
                    imgBlack.visibility = View.VISIBLE
                    imgRetake.visibility = View.VISIBLE
                } catch (ex:Exception) {
                    Log.d("errorfoto", ex.message.toString() + " " + currentPhotoPath)
                    Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                        // Ensure that there's a camera activity to handle the intent
                        takePictureIntent.resolveActivity(packageManager)?.also {
                            // Create the File where the photo should go
                            val photoFile: File? = try {
                                createImageFile()
                            } catch (ex: IOException) {
                                // Error occurred while creating the File

                                null
                            }
                            // Continue only if the File was successfully created
                            photoFile?.also {
                                val photoURI: Uri = FileProvider.getUriForFile(
                                    this,
                                    "com.pentamoo.andre.ksoapp",
                                    it
                                )
                                // takePictureIntent.putExtra("android.intent.extras.CAMERA_FACING", Camera.CameraInfo.CAMERA_FACING_FRONT)
                                //takePictureIntent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1);
                                //takePictureIntent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                            }
                        }
                    }
                }
            }
        }

        btnPlusPerson.setOnClickListener {
            var t:Int = txtNumPerson2.text.toString().toInt()
            t++
            jumperson = t
            txtNumPerson2.setText(t.toString())
            renderButton(jumperson)
        }

        btnMinAngpau.setOnClickListener {
            var t:Int = txtNumAngpau.text.toString().toInt()
            t--
            if(t <1) {
                t =1
                jumangpau = t
            }
            txtNumAngpau.setText(t.toString())
            renderAngpau(t)
        }

        btnMaxAngpau.setOnClickListener {
            var t:Int = txtNumAngpau.text.toString().toInt()
            t++
            jumangpau = t
            txtNumAngpau.setText(t.toString())
            renderAngpau(jumangpau)
        }

        btnMinPereson.setOnClickListener {
            var t:Int = txtNumPerson2.text.toString().toInt()
            t--

            if(t <1) {
                t =1
                jumperson = t
            }
            txtNumPerson2.setText(t.toString())
            renderButton(t)
        }

        btnPerson1.setOnClickListener {
            renderButton(1)
            txtNumPerson2.setText("1")
        }

        btnPerson2.setOnClickListener {
            renderButton(2)
            txtNumPerson2.setText("2")
        }

        btnPerson3.setOnClickListener {
            renderButton(3)
            txtNumPerson2.setText("3")
        }

        btnPerson4.setOnClickListener {
            renderButton(4)
            txtNumPerson2.setText("4")
        }

        btnPerson5.setOnClickListener {
            renderButton(5)
            txtNumPerson2.setText("5")
        }

        btnAngpau1.setOnClickListener {
            renderAngpau(1)
            txtNumAngpau.setText("1")
        }
        btnAngpau2.setOnClickListener {
            renderAngpau(2)
            txtNumAngpau.setText("2")
        }
        btnAngpau3.setOnClickListener {
            renderAngpau(3)
            txtNumAngpau.setText("3")
        }
        btnAngpau4.setOnClickListener {
            renderAngpau(4)
            txtNumAngpau.setText("4")
        }

        btnAngpau5.setOnClickListener {
            renderAngpau(5)
            txtNumAngpau.setText("5")
        }




        if(OptionsFragment.gateOpen) {
            btnInviteePresent.isEnabled = true
            txtGateClosedInfo.visibility = View.INVISIBLE
        }else {
            btnInviteePresent.isEnabled = false
            txtGateClosedInfo.visibility = View.VISIBLE
        }

        if(HomeActivity.user_roles == "event_manager") {
            disableAll()
        }


        btnInviteePresent.setOnClickListener {
            doAsync {
                val db =
                    Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
                var eventDao = db.eventDao()
                var inviteeDao = db.inviteeDao()
                var attendancesDao = db.attendancesDao()

                val sharedPrefs = applicationContext.getSharedPreferences(
                    "USERNAME", Context.MODE_PRIVATE
                )
                var username = sharedPrefs.getString("USERNAME", null)

                var event = eventDao.getId(inviteeDao.getInviteeById(i)[0].event_id!!)
                var eventid = event[0].id

                var tot:ArrayList<AttendancesModel> = attendancesDao.getTotalAttend(eventid, username) as ArrayList<AttendancesModel>
                var code = HomeActivity.initial + (tot.size+ 1)
                initial = HomeActivity.initial + (tot.size + 1)
              //  Utility.inviteeNext = Utility.inviteeNext + 1



                var inviteeId = i

                attendancesDao.insert(AttendancesModel(0, "", code, "", "", "", "", "manual","",1, 1,username,inviteeId,eventid, true))

                // update juga is attendingnya
                inviteeDao.updateAttending(1, inviteeId)

                uiThread {
                    contentView?.snackbar("Invitee Updated")
                    btnInviteePresent.visibility = View.INVISIBLE

                    txtGateClosedInfo.visibility = View.INVISIBLE
                    containerInvitee.visibility = View.VISIBLE
                    txtSide2.visibility = View.INVISIBLE
                    txtPhone.visibility = View.INVISIBLE
                    var tem = findViewById<TextView>(R.id.txtEmail)
                    tem.visibility = View.INVISIBLE

                    txtTable2.setBackgroundResource(R.drawable.tableborderall)

                    if(event[0].is_angpau!! == false) {
                        linearAngpau.visibility = View.INVISIBLE
                        textView8.visibility = View.INVISIBLE
                    }

                    if(event[0].is_person!! == false) {
                        linearPerson.visibility = View.INVISIBLE
                        textView3.visibility = View.INVISIBLE
                    }

                    if(event[0].is_note!! == false) {
                        txtDesc2.visibility = View.INVISIBLE
                        textView7.visibility = View.INVISIBLE
                    }

                }
            }
        }


        fabUpdate.setOnClickListener {
            var inviteeData:ArrayList<InviteeModel> = ArrayList()
            var attd:ArrayList<AttendancesModel> = ArrayList()

           // val intent = Intent(this, HomeActivity::class.java)
           // intent.putExtra("gatestatus", OptionsFragment.gateOpen)

            doAsync {
                val db =
                    Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
                var invDao = db.inviteeDao()

                inviteeData = invDao.getInviteeById(i) as ArrayList<InviteeModel>

                var attendancesDao = db.attendancesDao()
                var ang =0
                ang = txtNumAngpau.text.toString().toInt()


                var per =0
                    per = txtNumPerson2.text.toString().toInt()


                if(!currentPhotoPath.isEmpty()) {
                    attendancesDao.updateId(per, ang, txtDesc2.text.toString(), inviteeData[0].id)
                    attendancesDao.updatePhoto(currentPhotoPath, inviteeData[0].id)
                } else {
                    attendancesDao.updateId(per, ang, txtDesc2.text.toString(), inviteeData[0].id)
                }

                attd = attendancesDao.getId(inviteeData[0].id) as ArrayList<AttendancesModel>
                uiThread {
                    contentView?.snackbar("Invitee Updated")

                    if(Utility.isNetworkConnected(applicationContext)) {
                        // send to server
                        val queue = Volley.newRequestQueue(applicationContext)
                        val url = "http://jimmy.jitusolution.com/api/sendattendance"

                        val stringRequest = object : StringRequest(
                            Request.Method.POST, url,
                            Response.Listener<String> { response ->
                                val obj = JSONObject(response.toString())
                                if(obj.getString("status") == "success") {

                                    doAsync {
                                        val db =
                                            Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
                                        var attendancesDao = db.attendancesDao()

                                        attendancesDao.updateNeedSync(false, inviteeData[0].id)

                                        uiThread {

                                        }

                                    }
                                } },

                            Response.ErrorListener { response -> Log.d("erroratt", response.message.toString()) }) {
                            @Throws(AuthFailureError::class)
                            override fun getParams(): Map<String, String> {
                                val params = HashMap<String, String>()
                                params.put("uuid", uniqueID.toString())
                                params.put("email", HomeActivity.email)
                                params.put("tipe_scan", attd[0].tipe_scan.toString())
                                params.put("invitee_id", attd[0].invitee_id.toString())
                                params.put("user_username", attd[0].user_username.toString())
                                params.put("description", attd[0].description.toString())
                                params.put("total_angpau", attd[0].total_angpau.toString())
                                params.put("total_pax", attd[0].total_pax.toString())
                                params.put("invitee_event_id", attd[0].invitee_event_id.toString())
                                params.put("code", attd[0].code.toString())

                                return params
                            }
                        }

                        stringRequest.retryPolicy = DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
                        queue.add(stringRequest)


                        finish()
                       // startActivity(intent)


                    }

                }
            }

        }

        doAsync {
            val db =
                Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
            var attendancesDao = db.attendancesDao()
            var inviteeDao = db.inviteeDao()
            var data = ArrayList<InviteeModel>()
           if(qrcode != "") {
                data = inviteeDao.getInviteeByQR(qrcode) as ArrayList<InviteeModel>
                i = data[0].id
            } else {
                data = inviteeDao.getInviteeById(i) as ArrayList<InviteeModel>
            }

           // data = inviteeDao.getInviteeById(i) as ArrayList<InviteeModel>
            var att = attendancesDao.getId(i)
            var eventDao = db.eventDao()
            var event = eventDao.getId(inviteeDao.getInviteeById(i)[0].event_id!!)


            uiThread {
                supportActionBar?.title=data[0].title + " "  + data[0].name

                txtInviteeName2.text = data[0].title + " "  + data[0].name


               // accord.visibility = View.VISIBLE



                //Toast.makeText(applicationContext, event[0].is_angpau.toString(), Toast.LENGTH_LONG).show()
                if(event[0].is_angpau!! == false) {
                    linearAngpau.visibility = View.INVISIBLE
                    textView8.visibility = View.INVISIBLE
                }

                if(event[0].is_person!! == false) {
                    linearPerson.visibility = View.INVISIBLE
                    textView3.visibility = View.INVISIBLE
                }

                if(event[0].is_note!! == false) {
                    txtDesc2.visibility = View.INVISIBLE
                    textView7.visibility = View.INVISIBLE
                }

                //txtQR2.text = data[0].qr_code

                txtEmail.text = "Email: " + data[0].email
                txtPhone.text = "Phone: " +data[0].phone

                if(data[0].side == "L") {
                    txtSide2.text = "Side: Groom's Friend"
                } else if(data[0].side == "P") {
                    txtSide2.text = "Side: Bride's Friend"
                } else {
                    txtSide2.visibility = View.INVISIBLE
                }

                if(data[0].namatable != "") {
                    txtTable2.text = "Table: " + data[0].namatable
                } else {
                    txtTable2.visibility = View.INVISIBLE
                }



                if(att.size >0 ) {
                    photolink = currentPhotoPath
                    txtNumPerson2.setText(att[0].total_pax.toString())
                    //if(att[0].total_pax!!.toInt() >= 6) {
                      //  txtNumPerson2.setText(att[0].total_pax.toString())
                    //} else {
                      //  txtNumPerson2.setText("6")
                    //}
                  //  txtAngpau2.setText(att[0].total_angpau.toString())
                    txtDesc2.setText(att[0].description.toString())
                    //if(att[0].total_angpau!!.toInt() >= 6) {
                        txtNumAngpau.setText(att[0].total_angpau.toString())
                    //} else {
                      //  txtNumAngpau.setText("6")

                    //}

                    currentPhotoPath = att[0].photo_link.toString()
                    initial = att[0].code.toString()

                    Log.d("photo", currentPhotoPath)
                   // Toast.makeText(applicationContext, initial, Toast.LENGTH_LONG).show()

                    renderAngpau(att[0].total_angpau!!.toInt())
                    renderButton(att[0].total_pax!!.toInt())

                }


                // cek apakah ada attendance ?
                if(att.isNotEmpty()) {
                    //hide button present
                    btnInviteePresent.visibility = View.INVISIBLE
                    txtGateClosedInfo.visibility = View.INVISIBLE
                    containerInvitee.visibility = View.VISIBLE
                    txtSide2.visibility = View.INVISIBLE
                    txtPhone.visibility = View.INVISIBLE
                    txtEmail.visibility = View.INVISIBLE
                    txtTable2.setBackgroundResource(R.drawable.tableborderall)


                    txtTable2.setOnClickListener {
                        showInfo = !showInfo

                        if(showInfo) {


                            var topMargin = 0

                            var anim:Animation = object : Animation() {
                                override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                                    var cset:ConstraintSet = ConstraintSet()
                                    cset.clone(constraintTop)
                                    topMargin = (400 * interpolatedTime).toInt()
                                    cset.connect(containerInvitee.id, ConstraintSet.TOP, txtSide2.id, ConstraintSet.TOP, topMargin)
                                    cset.applyTo(constraintTop)

                                    if(interpolatedTime > 0.9f) {
                                        txtSide2.visibility = View.VISIBLE
                                        txtPhone.visibility = View.VISIBLE
                                        txtEmail.visibility = View.VISIBLE
                                        imageView13.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp)
                                    }
                                }


                            }
                            anim.duration = 500
                            constraintTop.startAnimation(anim)



                        } else {
                            var topMargin = 400

                            var anim:Animation = object : Animation() {
                                override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                                    var cset:ConstraintSet = ConstraintSet()
                                    cset.clone(constraintTop)
                                    topMargin = topMargin - (400 * interpolatedTime).toInt()
                                    cset.connect(containerInvitee.id, ConstraintSet.TOP, txtSide2.id, ConstraintSet.TOP, topMargin)
                                    cset.applyTo(constraintTop)

                                    if(interpolatedTime > 0.1f) {
                                        txtSide2.visibility = View.INVISIBLE
                                        txtPhone.visibility = View.INVISIBLE
                                        txtEmail.visibility = View.INVISIBLE

                                        imageView13.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp)
                                    }
                                }


                            }
                            anim.duration = 500
                            constraintTop.startAnimation(anim)


                        }
                    }
                }
            }
        }


    }
}
