package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.Room
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_event.*
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.uiThread

class EventActivity : AppCompatActivity() {

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> super.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event)

        setSupportActionBar(toolbar)
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#555555")))
        supportActionBar?.title="Choose Event"

        val sharedPrefs = applicationContext.getSharedPreferences(
            "CURRENT_EVENT_ID", Context.MODE_PRIVATE
        )
        var cureventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)

        cureventid = "0"
        val editor = sharedPrefs.edit()
        editor.putString("CURRENT_EVENT_ID", cureventid)
        editor.commit()



        doAsync {
            var x = 1
            val db =
                Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
            var eventDao = db.eventDao()
            var eventlist:ArrayList<EventModel> = eventDao.getAll() as ArrayList<EventModel>

            var adapter = EventAdapter(applicationContext, eventlist)



            uiThread {
                listEvent.adapter = adapter
               // longToast(x.toString())
                //window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                //progressBar.visibility = View.INVISIBLE
            }
        }
      //  supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
      //  supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}
