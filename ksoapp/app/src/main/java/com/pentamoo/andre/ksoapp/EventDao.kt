package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query

@Dao
interface EventDao {
    @Query("SELECT * from event")
    fun getAll(): List<EventModel>

    @Query("SELECT * from event WHERE id =:eventId")
    fun getId(eventId:Int): List<EventModel>

    @Insert(onConflict = REPLACE)
    fun insert(em: EventModel)

    @Delete
    fun delete(em: EventModel)

    @Query("DELETE FROM event")
    fun deleteAll()

    @Query("UPDATE event SET nama =:eventName, tanggal_mulai =:mulai, tanggal_selesai =:selesai, deskripsi=:eventDesc, roles=:eventRoles WHERE id =:eventId")
    fun update(eventId: Int, eventName: String, mulai: String, selesai: String, eventDesc:String, eventRoles:String)
}

