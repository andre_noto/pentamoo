package com.pentamoo.andre.ksoapp

import android.content.Context
import android.content.Intent
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

class EventAdapter : BaseAdapter {
    private var eventList = ArrayList<EventModel>()
    private var context: Context

    constructor(context: Context, eventList: ArrayList<EventModel>) : super() {
        this.eventList = eventList
        this.context = context
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val inflater: LayoutInflater
                = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rowView = inflater.inflate(R.layout.card_event, p2, false)

        var txtjudul = rowView.findViewById<TextView>(R.id.txtPicName)
        var txtdate= rowView.findViewById<TextView>(R.id.txtGate)
        var txtAssign = rowView.findViewById<TextView>(R.id.txtAssign)

        txtjudul.text = eventList[p0].nama.toString()

        var tglmulai = Utility.convertDate(eventList[p0].tanggal_mulai.toString())
        var tglselesai = Utility.convertDate(eventList[p0].tanggal_selesai.toString())

        if(eventList[p0].tanggal_mulai == eventList[p0].tanggal_selesai) {
            txtdate.text = tglmulai
        } else {
            txtdate.text = tglmulai+ " - " + tglselesai
        }

        txtAssign.text =  "You assigned as " + eventList[p0].roles

        var btn = rowView.findViewById<CardView>(R.id.cardEvent)
        btn.setOnClickListener {

            val sharedPrefs = context.getSharedPreferences(
                "CURRENT_EVENT_ID", Context.MODE_PRIVATE
            )
            var cureventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)

            val sharedPrefs2 = context.getSharedPreferences(
                "USERNAME", Context.MODE_PRIVATE
            )
            var username = sharedPrefs2.getString("USERNAME", null)

            cureventid = eventList[p0].id.toString()
            //context.longToast("event id " + cureventid)
            val editor = sharedPrefs.edit()
            editor.putString("CURRENT_EVENT_ID", cureventid)
            editor.commit()

            /*doAsync {
                var x = 1
                val db =
                    Room.databaseBuilder(context, AppDatabase::class.java, Utility.dbname).build()
                var eventDao = db.eventDao()
                var user = db.eventHasUserDao()
                var userList = user.getMyEventHasUser(username, cureventid.toInt())

                /*u = user.getMyEventHasUser(username.toString(), eventid.toInt()) as ArrayList<EventHasUserModel>
                var eventlist:ArrayList<EventModel> = eventDao.getId(eventid.toInt()) as ArrayList<EventModel>*/


                uiThread {
                    userList.forEach {
                        Log.d("cekuserit", it.user_username)
                    }
                }
            }*/

            //Log.d("errorcrash", eventList[p0].id.toString())
            val intent = Intent(context, HomeActivity::class.java)
            intent.putExtra("eventid", eventList[p0].id.toString())
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
            //((Activity) context).finish()
        }

        return rowView
    }

    override fun getItem(position: Int): Any {
        return eventList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return eventList.size
    }
}
