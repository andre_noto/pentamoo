package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "event")
data class EventModel(
    @PrimaryKey var id: Int,
    @ColumnInfo(name = "nama") var nama: String?,
    @ColumnInfo(name = "tanggal_mulai") var tanggal_mulai: String?,
    @ColumnInfo(name = "tanggal_selesai") var tanggal_selesai: String?,
    @ColumnInfo(name = "deskripsi") var deskprisi: String?,
    @ColumnInfo(name = "roles") var roles: String?,
    @ColumnInfo(name = "initial") var initial: String?,
    @ColumnInfo(name = "is_note") var is_note: Boolean?,
    @ColumnInfo(name = "is_angpau") var is_angpau: Boolean?,
    @ColumnInfo(name = "is_person") var is_person: Boolean?




    )

