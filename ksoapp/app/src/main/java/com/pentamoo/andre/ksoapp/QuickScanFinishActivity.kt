package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Matrix
import android.hardware.Camera
import android.support.media.ExifInterface
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.util.Log
import android.widget.Toast
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_quick_scan_finish.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random

class QuickScanFinishActivity : AppCompatActivity() {
    companion object {
        var sudahfoto = false
    }
    var currentPhotoPath  = ""
    val REQUEST_TAKE_PHOTO = 1
    var eventid:String = ""
    var invid = 0
    var initial =""
    var jumperson =0

    private val PREF_UNIQUE_ID = "PREF_UNIQUE_ID"

    private var uniqueID: String? = null


    @Synchronized
    fun id(context: Context): String {
        if (uniqueID == null) {
            val sharedPrefs = context.getSharedPreferences(
                PREF_UNIQUE_ID, Context.MODE_PRIVATE
            )
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null)

            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString()
                val editor = sharedPrefs.edit()
                editor.putString(PREF_UNIQUE_ID, uniqueID)
                editor.commit()
            }
        }

        return uniqueID as String
    }


    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        //  val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "foto_${initial}", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
            Log.d("currentphotopath", currentPhotoPath)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            Log.d("cekcurrentphotopathyess", currentPhotoPath)
            try{
                val f = File(currentPhotoPath)
                var x = Uri.fromFile(f)
                var bmp:Bitmap = MediaStore.Images.Media.getBitmap(contentResolver, x)

                var ei:ExifInterface = ExifInterface(currentPhotoPath)
                var i  = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)

                //var rotatedBitmap = null
                when(i) {
                    ExifInterface.ORIENTATION_ROTATE_90 ->  imageView10.setImageBitmap(rotateImage(bmp, 90f))
                    ExifInterface.ORIENTATION_ROTATE_180 -> imageView10.setImageBitmap(rotateImage(bmp, 180f))
                    ExifInterface.ORIENTATION_ROTATE_270 -> imageView10.setImageBitmap(rotateImage(bmp, 270f))
                    ExifInterface.ORIENTATION_NORMAL ->
                        imageView10.setImageBitmap(bmp)

                }

            } catch (e:Exception) {
                sudahfoto = true
            }



          //  var matrix:Matrix = Matrix()
           // matrix.postRotate(angle);
            //imageView10.setImageBitmap(bmp)
        }
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quick_scan_finish)

        eventid = intent.getStringExtra("eventid")
        Log.d("eventid", eventid)
        invid = intent.getIntExtra("invid", 0)
        initial = intent.getStringExtra("initial")
        jumperson = intent.getIntExtra("jumperson",1)

        buttonFinish.setOnClickListener {
            var attd:ArrayList<AttendancesModel> = ArrayList()

            doAsync {
                val db =
                    Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
                var attendancesDao = db.attendancesDao()
                var eventDao = db.eventDao()
                var inviteeDao = db.inviteeDao()
                var cek = attendancesDao.getId(invid)
                if(cek.size > 0 ) {
                    attendancesDao.updatePhoto(currentPhotoPath, invid)
                } else {
                    //insert
                    val sharedPrefs = applicationContext.getSharedPreferences(
                        "USERNAME", Context.MODE_PRIVATE
                    )
                    var username = sharedPrefs.getString("USERNAME", null)

                    var event = eventDao.getId(eventid.toInt())
                    var tot:ArrayList<AttendancesModel> = attendancesDao.getTotalAttend(eventid.toInt(), username) as ArrayList<AttendancesModel>
                    var code = event[0].initial + (tot.size+ 1)


                    attendancesDao.insert(AttendancesModel(0, "", code, "", "", "", "", "scan",currentPhotoPath,jumperson, 1,username,invid,eventid.toInt(), true))
                    inviteeDao.updateAttending(1, invid)

                    attd = attendancesDao.getId(invid) as ArrayList<AttendancesModel>

                }
                uiThread {

                    if(Utility.isNetworkConnected(applicationContext) && attd.size >0 ) {
                        // send to server
                        val queue = Volley.newRequestQueue(applicationContext)
                        val url = "http://jimmy.jitusolution.com/api/sendattendance"

                        val stringRequest = object : StringRequest(
                            Request.Method.POST, url,
                            Response.Listener<String> { response ->
                                val obj = JSONObject(response.toString())
                                if(obj.getString("status") == "success") {

                                    doAsync {
                                        val db =
                                            Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
                                        var attendancesDao = db.attendancesDao()

                                        attendancesDao.updateNeedSync(false, invid)

                                    }
                                } },

                            Response.ErrorListener { response -> Log.d("erroratt", response.message.toString()) }) {
                            @Throws(AuthFailureError::class)
                            override fun getParams(): Map<String, String> {
                                val params = HashMap<String, String>()
                                params.put("uuid", uniqueID.toString())
                                params.put("email", HomeActivity.email)
                                params.put("tipe_scan", attd[0].tipe_scan.toString())
                                params.put("invitee_id", attd[0].invitee_id.toString())
                                params.put("user_username", attd[0].user_username.toString())
                                params.put("description", attd[0].description.toString())
                                params.put("total_angpau", attd[0].total_angpau.toString())
                                params.put("total_pax", attd[0].total_pax.toString())
                                params.put("invitee_event_id", attd[0].invitee_event_id.toString())
                                params.put("code", attd[0].code.toString())

                                return params
                            }
                        }

                        stringRequest.retryPolicy = DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
                        queue.add(stringRequest)
                    }

                    sudahfoto = false
                    val intent = Intent(applicationContext, QuickScanActivity::class.java)
                    intent.putExtra("eventid", eventid.toInt())
                    //intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                    startActivity(intent)


                    Log.d("cekuithread", " ok")

                }
            }
        }

        fabRetake.setOnClickListener {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                // Ensure that there's a camera activity to handle the intent
                takePictureIntent.resolveActivity(packageManager)?.also {
                    // Create the File where the photo should go
                    val photoFile: File? = try {
                        createImageFile()
                    } catch (ex: IOException) {
                        // Error occurred while creating the File

                        null
                    }
                    // Continue only if the File was successfully created
                    photoFile?.also {
                        val photoURI: Uri = FileProvider.getUriForFile(
                            this,
                            "com.pentamoo.andre.ksoapp",
                            it
                        )
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        takePictureIntent.putExtra("android.intent.extras.CAMERA_FACING", Camera.CameraInfo.CAMERA_FACING_FRONT)
                        takePictureIntent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1)
                        takePictureIntent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true)

                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                    }
                }
            }
        }

        if(!sudahfoto) {
            sudahfoto = true
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                // Ensure that there's a camera activity to handle the intent
                takePictureIntent.resolveActivity(packageManager)?.also {
                    // Create the File where the photo should go
                    val photoFile: File? = try {
                        createImageFile()
                    } catch (ex: IOException) {
                        // Error occurred while creating the File

                        null
                    }
                    // Continue only if the File was successfully created
                    photoFile?.also {
                        val photoURI: Uri = FileProvider.getUriForFile(
                            this,
                            "com.pentamoo.andre.ksoapp",
                            it
                        )
                        takePictureIntent.putExtra("android.intent.extras.CAMERA_FACING", Camera.CameraInfo.CAMERA_FACING_FRONT)
                        takePictureIntent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1);
                        takePictureIntent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                    }
                }
            }
        }
    }
}
