package com.pentamoo.andre.ksoapp


import android.app.Dialog
import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_dialog_manager_password.*
import kotlinx.android.synthetic.main.fragment_options.*
import org.jetbrains.anko.*
import org.jetbrains.anko.design.snackbar
import org.json.JSONArray
import org.json.JSONObject


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class OptionsFragment : Fragment() {
    companion object {
        var context: Context?= null
        var gateOpen:Boolean = false
        var gateChanged:Boolean =false
        fun newInstance(): OptionsFragment {
            return OptionsFragment()
        }
    }

    fun refreshFragment() {
        if(HomeActivity.user_roles == "registrator"  || HomeActivity.user_roles == "event_manager") {
            view?.findViewById<Button>(R.id.btnCloseOpenGate)?.visibility = View.INVISIBLE
            view?.findViewById<Button>(R.id.btnBackToSelectEvent3)?.visibility = View.INVISIBLE

        } else {
            view?.findViewById<Button>(R.id.btnCloseOpenGate)?.visibility = View.VISIBLE

            view?.findViewById<Button>(R.id.btnBackToSelectEvent3)?.visibility = View.VISIBLE
        }
    }

    override fun onStart() {
        super.onStart()
       // activity?.longToast("here")
        if(HomeActivity.user_roles == "registrator" || HomeActivity.user_roles == "event_manager") {
            view?.findViewById<Button>(R.id.btnCloseOpenGate)?.visibility = View.INVISIBLE

            view?.findViewById<Button>(R.id.btnBackToSelectEvent3)?.visibility = View.INVISIBLE
        } else    {
            view?.findViewById<Button>(R.id.btnCloseOpenGate)?.visibility = View.VISIBLE

            view?.findViewById<Button>(R.id.btnBackToSelectEvent3)?.visibility = View.VISIBLE
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var v =  inflater.inflate(R.layout.fragment_options, container, false)
        var hasil:ArrayList<EventHasUserModel> = ArrayList()

        val sharedPrefs = context!!.getSharedPreferences(
            "CURRENT_EVENT_ID", Context.MODE_PRIVATE
        )
        var cureventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)

        val sharedPrefs2 = context!!.getSharedPreferences(
            "USERNAME", Context.MODE_PRIVATE
        )
        var username = sharedPrefs2.getString("USERNAME", null)

        if(HomeActivity.user_roles == "registrator") {
            view?.findViewById<Button>(R.id.btnCloseOpenGate)?.visibility = View.INVISIBLE
        } else {
            view?.findViewById<Button>(R.id.btnCloseOpenGate)?.visibility = View.VISIBLE
        }

        var quick = v.findViewById<Button>(R.id.btnBackToSelectEvent3)

        quick.setOnClickListener {
            val intent = Intent(context!!, QuickScanActivity::class.java)
            intent.putExtra("eventid", cureventid.toInt())
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            activity?.finish()
            context!!.startActivity(intent)
        }

        // update last sync and check opengate or close gate
        doAsync {
            try {
                val db =
                    Room.databaseBuilder(context!!, AppDatabase::class.java, Utility.dbname).build()
                var eventHasUserDao = db.eventHasUserDao()

                hasil = eventHasUserDao.getMyEventHasUser( username.toString(), cureventid.toInt()) as ArrayList<EventHasUserModel>

            } catch (ex:Exception) {
                //v.snackbar("Something wrong ...")
            }

            uiThread {

                Log.d("cekusernameeventid", hasil.size.toString())
                //Log.d("cekusernameeventid", username.toString())

                //Log.d("cekusernameeventid", hasil[0].user_username.toString())
              //  Log.d("cekusernameeventid", hasil[1].user_username.toString())
                var tls = v.findViewById<TextView>(R.id.txtLastSync)
                var btnOC = v.findViewById<Button>(R.id.btnCloseOpenGate)
                if(hasil.size.toInt() > 0) {
                    if(hasil[0].open_gate == true) {
                        gateOpen = true
                        btnOC.setText("CLOSE GATE")
                    } else {
                        gateOpen = false
                        btnOC.setText("OPEN GATE")
                    }

                    if (hasil[0].last_sync.toString() == "null") {

                        tls.text = "Last sync: never"
                    } else {
                        tls.text = "Last sync was " + hasil[0].last_sync.toString()
                    }
                }
            }

        }

        var bocg = v.findViewById<Button>(R.id.btnCloseOpenGate)
        bocg.setOnClickListener {
            var dialogs = Dialog(activity)
            dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialogs.setCancelable(true)
            dialogs.setContentView(R.layout.fragment_dialog_manager_password)
            var b = dialogs.findViewById<Button>(R.id.btnOpenCloseGate)
            var te = dialogs.findViewById<EditText>(R.id.txtEmail)
            var tp = dialogs.findViewById<EditText>(R.id.txtPass)
            var t = dialogs.findViewById<TextView>(R.id.txtInvalid)

            if(gateOpen == false) {
                b.setText("OPEN GATE")
            } else {
                b.setText("CLOSE GATE")
            }

            b.setOnClickListener {
                var shapass =""
                var pass = tp.text.toString()
                var email = te.text.toString()

                doAsync {
                    val db =
                        Room.databaseBuilder(context!!, AppDatabase::class.java, Utility.dbname).build()
                    var eventHasUser = db.eventHasUserDao()
                    var em = eventHasUser.getMyEventHasUserByEventManager(email)

                    var h = Utility.hashString("MD5", em[0].salt.toString())
                    shapass = Utility.hashString("MD5", pass + h)


                    uiThread {

                        var h = Utility.hashString("MD5", em[0].salt.toString())
                        var p = tp.text.toString() + h
                        //context?.toast(tp.text)
                       if(em[0].password  ==  Utility.hashString("MD5", p)) {
                           gateOpen = !gateOpen
                            gateChanged = true
                           if(gateOpen ==false) {
                               bocg.setText("OPEN GATE")
                               v.snackbar("Gate is Close")
                           } else {
                               bocg.setText("CLOSE GATE")
                               v.snackbar("Gate is Open")
                           }
                           dialogs.dismiss()

                       } else {
                           t.visibility = View.VISIBLE
                       }

                       // context?.toast(Utility.hashString("MD5", em[0].salt.toString()))
                     //   context?.toast("cek" + shapass + " = " + em[0].password)
                    }
                }


             //  context?.toast("cek" + te.text.toString())


               // dialogs.dismiss()
            }
            dialogs.show()
        }

        var bls = v.findViewById<Button>(R.id.btnSync)
        bls.setOnClickListener {
            var hasil:ArrayList<AttendancesModel> = ArrayList()
            var pb=  v.findViewById<ProgressBar>(R.id.progressBar2)
            pb.visibility = View.VISIBLE
            activity?.window?.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

            var hasil2:ArrayList<EventHasUserModel> = ArrayList()
            if(HomeActivity.user_roles== "event_manager") {
                doAsync {
                    try {
                        val db =
                            Room.databaseBuilder(context!!, AppDatabase::class.java, Utility.dbname).build()
                        var eventHasUserDao = db.eventHasUserDao()

                        hasil2 = eventHasUserDao.getIdWithRoles(cureventid.toInt(), "receptionist") as ArrayList<EventHasUserModel>


                    } catch (ex:Exception) {
                        //v.snackbar("Something wrong ...")
                    }
                    uiThread {
                        val queue = Volley.newRequestQueue(context)
                        val url = "http://jimmy.jitusolution.com/api/sendopengatestatus"

                        val stringRequest = object : StringRequest(
                            Request.Method.POST, url,
                            Response.Listener<String> { response ->
                                pb.visibility = View.INVISIBLE
                                activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                                try {
                                    val obj = JSONObject(response.toString())
                                    Log.d("hasilog", obj.toString())
                                    var ls = obj.getString("lastsync")


                                    doAsync {
                                        val db =
                                            Room.databaseBuilder(context!!, AppDatabase::class.java, Utility.dbname).build()
                                        var eventHasUserDao = db.eventHasUserDao()
                                        eventHasUserDao.updateLastSync(ls, username, cureventid.toInt())

                                        uiThread {
                                            var tls = v.findViewById<TextView>(R.id.txtLastSync)
                                            tls.text = "Last Sync: " + ls
                                            v.snackbar("Sync success")

                                            getinvitee()
                                        }
                                    }
                                    Log.d("senddatacek",response.toString())

                                } catch(e:Exception) {
                                    Log.d("senddatacek", e.message.toString())
                                }
                            },
                            Response.ErrorListener { Log.d("senddatacek", it.message.toString())   }){
                            @Throws(AuthFailureError::class)
                            override fun getParams(): Map<String, String> {
                                val params = HashMap<String, String>()

                                Log.d("cekhasil2", hasil2.size.toString())

                                val care_type = JSONArray()
                                for(i in 0 until hasil2.size) {

                                    val rootObject= JSONObject()
                                    rootObject.put("open_gate",hasil2[i].open_gate)
                                    rootObject.put("user_username",hasil2[i].user_username)
                                    care_type.put(rootObject)
                                }

                                params["care_type"] = care_type.toString()
                                params["eventid"] = cureventid.toString()
                                return params
                            }
                        }

                        stringRequest.retryPolicy = DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
                        queue.add(stringRequest)

                    }
                }
            } else {
                doAsync {
                    try {
                        val db =
                            Room.databaseBuilder(context!!, AppDatabase::class.java, Utility.dbname).build()
                        var attendancesDao = db.attendancesDao()

                        val sharedPrefs = activity!!.applicationContext.getSharedPreferences(
                            "USERNAME", Context.MODE_PRIVATE
                        )
                        var username = sharedPrefs.getString("USERNAME", null)

                        var recent = attendancesDao.getAllRecent(cureventid.toInt(), username  )

                        hasil = attendancesDao.getAllRecent( cureventid.toInt(), username) as ArrayList<AttendancesModel>

                    } catch (ex:Exception) {
                        //v.snackbar("Something wrong ...")
                    }
                    uiThread {
                        Log.d("cekatt", hasil.size.toString())

                        val queue = Volley.newRequestQueue(context)
                        val url = "http://jimmy.jitusolution.com/api/sendattendancedata"
                        // Request a string response from the provided URL.
                        val stringRequest = object : StringRequest(
                            Request.Method.POST, url,
                            Response.Listener<String> { response ->
                                pb.visibility = View.INVISIBLE
                                activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                                try {
                                    val obj = JSONObject(response.toString())
                                    var ls = obj.getString("lastsync")


                                    doAsync {
                                        val db =
                                            Room.databaseBuilder(context!!, AppDatabase::class.java, Utility.dbname).build()
                                        var eventHasUserDao = db.eventHasUserDao()
                                        eventHasUserDao.updateLastSync(ls, username, cureventid.toInt())

                                        uiThread {
                                            var tls = v.findViewById<TextView>(R.id.txtLastSync)
                                            tls.text = "Last Sync: " + ls
                                            v.snackbar("Sync success")

                                            getinvitee()
                                        }
                                    }
                                    Log.d("senddatacek",response.toString())

                                } catch(e:Exception) {
                                    Log.d("senddatacek", e.message.toString())
                                }
                            },
                            Response.ErrorListener { Log.d("senddatacek", it.message.toString())   }){
                            @Throws(AuthFailureError::class)
                            override fun getParams(): Map<String, String> {
                                val params = HashMap<String, String>()


                                val care_type = JSONArray()

                                for(i in 0 until hasil.size) {
                                    val rootObject= JSONObject()
                                    rootObject.put("id",hasil[i].id)
                                    rootObject.put("created_at",hasil[i].created_at)
                                    rootObject.put("code",hasil[i].code)
                                    rootObject.put("deleted_at",hasil[i].deleted_at)
                                    rootObject.put("updated_at",hasil[i].updated_at)
                                    rootObject.put("description",hasil[i].description)
                                    rootObject.put("tipe_scan",hasil[i].tipe_scan)
                                    rootObject.put("photo_link",hasil[i].photo_link)
                                    rootObject.put("total_pax",hasil[i].total_pax)
                                    rootObject.put("total_angpau",hasil[i].total_angpau)
                                    rootObject.put("user_username",hasil[i].user_username)
                                    rootObject.put("invitee_id",hasil[i].invitee_id)
                                    rootObject.put("invitee_event_id",hasil[i].invitee_event_id)
                                    care_type.put(rootObject)
                                }

                                params["care_type"] = care_type.toString()
                                params["username"] = username.toString()
                                params["eventid"] = cureventid.toString()
                                if(gateChanged) {
                                    params["gatestatus"] = gateOpen.toString()
                                    gateChanged = false
                                }
                                return params
                            }
                        }

                        stringRequest.retryPolicy = DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
                        queue.add(stringRequest)


                        /// 1. sned gate status
                        // 2. get data invitee + attendacne
                    }
                }

            }

        }

        var tn = v.findViewById<Button>(R.id.btnSignOut)
        tn.setOnClickListener {
            val sharedPrefs = context!!.getSharedPreferences(
                "CURRENT_EVENT_ID", Context.MODE_PRIVATE
            )
            val editor = sharedPrefs.edit()
            editor.remove("CURRENT_EVENT_ID")
            editor.commit()

            val sharedPrefs2 = context!!.getSharedPreferences(
                "USERNAME", Context.MODE_PRIVATE
            )
            val editor2 = sharedPrefs2.edit()
            editor2.remove("USERNAME")
            editor2.commit()

            val intent = Intent(context!!, MainActivity::class.java)
            // intent.putExtra("eventid", eventList[p0].id)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context!!.startActivity(intent)

            activity?.finish()
        }
        return v
    }

    private fun getgatestatus() {
        val queue = Volley.newRequestQueue(context)
        val url = "http://jimmy.jitusolution.com/api/getgatestatus"

        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            Response.Listener<String> { response ->
                //pb.visibility = View.INVISIBLE
                activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                try {
                    val obj = JSONObject(response.toString())
                    if(obj.getString("status") == "success") {
                        // get events list and save to database

                        val arr = (obj.getString("data"))

                        if(arr == "1") {
                            OptionsFragment.gateOpen = true
                            var bocg = view?.findViewById<Button>(R.id.btnCloseOpenGate)
                            bocg?.text = "CLOSE GATE"

                        } else {
                            OptionsFragment.gateOpen = false
                            var bocg = view?.findViewById<Button>(R.id.btnCloseOpenGate)
                            bocg?.text = "OPEN GATE"
                        }

                        Log.d("gatecek",OptionsFragment.gateOpen.toString())
                        /*var ehm:ArrayList<EventHasUserModel> = ArrayList(arr.length())
                        for(i in 0 until arr.length()) {
                            var objx = arr.getJSONObject(i)


                            var o = EventHasUserModel(objx.getInt("id"),
                                objx.getString("name"),
                                objx.getString("code"), objx.getString("address"),
                                objx.getString("email"), objx.getString("description"),
                                objx.optInt("is_attending", 0), objx.getString("message"),
                                objx.optInt("num_of_persons", 0),objx.getString("phone"),
                                objx.getString("side"), objx.getString("status"),
                                objx.getString("tag"), objx.getString("title"), objx.getInt("event_id"),
                                objx.getString("namatable"),
                                objx.getString("qr_code"))
                            ehm.add(o)
                        }

                        doAsync {
                            try {
                                var x = 1
                                val db =
                                    Room.databaseBuilder(context!!.applicationContext, AppDatabase::class.java, Utility.dbname).build()
                                var inviteeDao = db.inviteeDao()

                                inviteeDao.deleteAll()

                                for (objim in im) {
                                    inviteeDao.insert(objim)
                                }
                            } catch(e:Exception) {
                                context?.longToast("error" +e.message.toString())
                            }
                            uiThread {
                                val sharedPrefs = context!!.getSharedPreferences(
                                    "CURRENT_EVENT_ID", Context.MODE_PRIVATE
                                )
                                var cureventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)

                                val sharedPrefs2 = context!!.getSharedPreferences(
                                    "USERNAME", Context.MODE_PRIVATE
                                )
                                var username = sharedPrefs2.getString("USERNAME", null)

                                getattendances(cureventid.toString())
                                // longToast(x.toString())
                                /*window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                                progressBar.visibility = View.INVISIBLE

                                val intent = Intent(it, EventActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                                startActivity(intent)

                                finish()*/
                            }
                        }
*/
                    } else if(obj.getString("status") == "failed") {
                        view?.snackbar("Unable to retrieve event data")
                        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                       // progressBar.visibility = View.INVISIBLE
                    }

                } catch(e:Exception) {
                    context?.longToast("erroexecption"+e.message.toString())
                    activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    //progressBar.visibility = View.INVISIBLE
                }

                activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                // progressBar.visibility = View.INVISIBLE
            },
            Response.ErrorListener { Log.d("senddatacek", it.message.toString())   }){
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()


                val sharedPrefs = context!!.getSharedPreferences(
                    "CURRENT_EVENT_ID", Context.MODE_PRIVATE
                )
                var cureventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)


                val sharedPrefs2 = context!!.getSharedPreferences(
                    "USERNAME", Context.MODE_PRIVATE
                )
                var username = sharedPrefs2.getString("USERNAME", null)


                params["eventid"] = cureventid.toString()

                params["username"] = username
                return params
            }
        }

        stringRequest.retryPolicy = DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        queue.add(stringRequest)

    }

    private fun getinvitee(){
        val queue = Volley.newRequestQueue(context)
        val url = "http://jimmy.jitusolution.com/api/getinviteeonly"
        // Request a string response from the provided URL.
        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            Response.Listener<String> { response ->
                //pb.visibility = View.INVISIBLE
                activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                try {
                    val obj = JSONObject(response.toString())
                    // Log.d("inviteecek","response")
                    if(obj.getString("status") == "success") {
                        // get events list and save to database

                        val arr = JSONArray(obj.getString("data"))
                        var im:ArrayList<InviteeModel> = ArrayList(arr.length())
                        for(i in 0 until arr.length()) {
                            var objx = arr.getJSONObject(i)


                            var o = InviteeModel(objx.getInt("id"),
                                objx.getString("name"),
                                objx.getString("code"), objx.getString("address"),
                                objx.getString("email"), objx.getString("description"),
                                objx.optInt("is_attending", 0), objx.getString("message"),
                                objx.optInt("num_of_persons", 0),objx.getString("phone"),
                                objx.getString("side"), objx.getString("status"),
                                objx.getString("tag"), objx.getString("title"), objx.getInt("event_id"),
                                objx.getString("namatable"),
                                objx.getString("qr_code"))
                            im.add(o)
                        }

                        doAsync {
                            try {
                                var x = 1
                                val db =
                                    Room.databaseBuilder(context!!.applicationContext, AppDatabase::class.java, Utility.dbname).build()
                                var inviteeDao = db.inviteeDao()

                                inviteeDao.deleteAll()

                                for (objim in im) {
                                    inviteeDao.insert(objim)
                                }
                            } catch(e:Exception) {
                                context?.longToast("error" +e.message.toString())
                            }
                            uiThread {
                                val sharedPrefs = context!!.getSharedPreferences(
                                    "CURRENT_EVENT_ID", Context.MODE_PRIVATE
                                )
                                var cureventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)

                                val sharedPrefs2 = context!!.getSharedPreferences(
                                    "USERNAME", Context.MODE_PRIVATE
                                )
                                var username = sharedPrefs2.getString("USERNAME", null)

                                getattendances(cureventid.toString())
                                // longToast(x.toString())
                                /*window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                                progressBar.visibility = View.INVISIBLE

                                val intent = Intent(it, EventActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                                startActivity(intent)

                                finish()*/
                            }
                        }

                    } else if(obj.getString("status") == "failed") {
                        view?.snackbar("Unable to retrieve event data")
                        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                        progressBar.visibility = View.INVISIBLE
                    }
                } catch(e:Exception) {
                    context?.longToast("erroexecption"+e.message.toString())
                    activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    progressBar.visibility = View.INVISIBLE
                }

                activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                // progressBar.visibility = View.INVISIBLE
            },
            Response.ErrorListener { Log.d("senddatacek", it.message.toString())   }){
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()


                val sharedPrefs = context!!.getSharedPreferences(
                    "CURRENT_EVENT_ID", Context.MODE_PRIVATE
                )
                var cureventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)


                params["eventid"] = cureventid.toString()
                return params
            }
        }

        stringRequest.retryPolicy = DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        queue.add(stringRequest)
    }


    private fun getattendances(kodeevent:String) {

        val queue = Volley.newRequestQueue(context)
        val url = "http://jimmy.jitusolution.com/api/getattendancesonly"

        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            Response.Listener<String> { response ->
                try {

                    // Log.d("eventhasuser",response.toString())
                    val obj = JSONObject(response.toString())
                    if(obj.getString("status") == "success") {
                        Log.d("hasilatt2", obj.toString())
                        // get events list and save to database

                        val arr = JSONArray(obj.getString("data"))
                        //var arr = JSONArray(arrx.get(0))


                        Log.d("testatt", arr.length().toString())
                        var amo:ArrayList<AttendancesModel> = ArrayList()
                        for(i in 0 until arr.length()) {

                            var temp  = arr.getJSONArray(i)

                            Log.d("testatt2" , "datake " +  + i + " " + temp.toString())

                            for(j in 0 until temp.length()) {
                                var obj = temp.getJSONObject(j)
                                var am = AttendancesModel(0,obj.getString("created_at"), obj.getString("code"),
                                    obj.getString("date"), obj.getString("deleted_at"), obj.getString("updated_at"),
                                    obj.getString("description"), obj.getString("tipe_scan"), obj.getString("photo_link"),
                                    obj.getInt("total_pax"), obj.getInt("total_angpau"),
                                    obj.getString("user_username"), obj.getInt("invitee_id"), obj.getInt("invitee_event_id"),false);
                                Log.d("testamo", am.toString())
                                amo.add(am)
                            }

                        }

                        doAsync {
                            try {
                                var x = 1
                                val db =
                                    Room.databaseBuilder(context!!.applicationContext, AppDatabase::class.java, Utility.dbname).build()

                                var attendancesDao = db.attendancesDao()
                                var inviteeDao = db.inviteeDao()
                                attendancesDao.deleteAll()

                                /// jangan lupa ambil data attendanes


                                for (i in 0 until amo.size) {
                                    attendancesDao.insert(amo[i])
                                    // update juga is attendingnya
                                    inviteeDao.updateAttending(1, amo[i].invitee_id!!.toInt())

                                }

                                //cekHasil = eventHasUserDao.getAll() as ArrayList<EventHasUserModel>
                            } catch(e:Exception) {
                                context?.longToast("error" +e.message.toString())
                            }
                            uiThread {
                                // longToast(x.toString())
                                Log.d("amo", amo.size.toString())
                                getgatestatus()
                                //getattendances(im)
                                activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

                            }
                        }

                    } else if(obj.getString("status") == "failed") {
                        view?.snackbar("Unable to retrieve attendance data")
                        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                        progressBar.visibility = View.INVISIBLE
                    }
                } catch(e:Exception) {
                    context?.longToast("erroexecption"+e.message.toString())
                    activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    progressBar.visibility = View.INVISIBLE
                }

                activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                // progressBar.visibility = View.INVISIBLE
            },
            Response.ErrorListener {  context?.longToast("errorvolley" +it.message.toString())
                activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                progressBar.visibility = View.INVISIBLE }){
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = java.util.HashMap<String, String>()

                params.put("events", kodeevent)
                return params
            }
        }

        stringRequest.retryPolicy = DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        queue.add(stringRequest)
    }


}
