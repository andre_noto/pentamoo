package com.pentamoo.andre.ksoapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_quick_scan.*


class QuickScanActivity : AppCompatActivity() {

    override fun onBackPressed() {
        //super.onBackPressed()
        Toast.makeText(this, "Back key is not allowed", Toast.LENGTH_SHORT).show()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quick_scan)

        var id = intent.getIntExtra("eventid", 0)

        imgScreen.setOnClickListener {
            val intent = Intent(this, QRActivity::class.java)
             intent.putExtra("cameraid", 1)
            //intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
            startActivity(intent)
        }

        imgScreen.setOnLongClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            intent.putExtra("navigate", R.id.navigation_invitee_list)
            //intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY


            startActivity(intent)
          //  this.finish()
            true
        }

        if(id != 0) {
            Picasso.get().load("http://jimmy.jitusolution.com/screen/" + id + ".jpg").memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).into(imgScreen)
        }
    }
}
