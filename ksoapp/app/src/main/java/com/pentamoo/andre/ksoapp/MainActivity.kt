package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.View
import android.view.WindowManager
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.contentView
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    private var uniqueID: String? = null
    private val PREF_UNIQUE_ID = "PREF_UNIQUE_ID"

    @Synchronized
    fun id(context: Context): String {
        if (uniqueID == null) {
            val sharedPrefs = context.getSharedPreferences(
                PREF_UNIQUE_ID, Context.MODE_PRIVATE
            )
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null)

            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString()
                val editor = sharedPrefs.edit()
                editor.putString(PREF_UNIQUE_ID, uniqueID)
                editor.commit()
            }
        }

        return uniqueID as String
    }

    private fun getattendances(im:ArrayList<EventHasUserModel>) {
        var kodeevent = ""
        im.forEach {
           kodeevent = kodeevent + it.event_id + ","
        }

        val queue = Volley.newRequestQueue(this)
        val url = "http://jimmy.jitusolution.com/api/getattendances"

        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            Response.Listener<String> { response ->
                try {

                    // Log.d("eventhasuser",response.toString())
                    val obj = JSONObject(response.toString())
                    if(obj.getString("status") == "success") {
                        Log.d("hasilatt", obj.toString())
                        // get events list and save to database

                        val arr = JSONArray(obj.getString("data"))
                        //var arr = JSONArray(arrx.get(0))


                        Log.d("testatt", arr.length().toString())
                        var amo:ArrayList<AttendancesModel> = ArrayList()
                        for(i in 0 until arr.length() -1) {

                            var temp  = arr.getJSONArray(i)

                            Log.d("testatt" , "datake " +  + i + " " + temp.toString())

                            for(j in 0 until temp.length()) {
                                var obj = temp.getJSONObject(j)
                                var am = AttendancesModel(0,obj.getString("created_at"), obj.getString("code"),
                                    obj.getString("date"), obj.getString("deleted_at"), obj.getString("updated_at"),
                                        obj.getString("description"), obj.getString("tipe_scan"), obj.getString("photo_link"),
                                    obj.getInt("total_pax"), obj.getInt("total_angpau"),
                                    obj.getString("user_username"), obj.getInt("invitee_id"), obj.getInt("invitee_event_id"), false);

                                amo.add(am)
                            }

                        }

                        doAsync {
                            try {
                                var x = 1
                                val db =
                                    Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()

                                var attendancesDao = db.attendancesDao()
                                var inviteeDao = db.inviteeDao()
                                attendancesDao.deleteAll()

                                /// jangan lupa ambil data attendanes

                                for (i in 0 until amo.size) {
                                    attendancesDao.insert(amo[i])
                                    // update juga is attendingnya
                                    inviteeDao.updateAttending(1, amo[i].invitee_id!!.toInt())

                                }

                                //cekHasil = eventHasUserDao.getAll() as ArrayList<EventHasUserModel>
                            } catch(e:Exception) {
                                longToast("error" +e.message.toString())
                            }
                            uiThread {
                                // longToast(x.toString())

                                //getattendances(im)
                                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                                progressBar.visibility = View.INVISIBLE

                              // Log.d("eventhasuser","cekhasil" +cekHasil.size.toString())

                                val intent = Intent(it, EventActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                                startActivity(intent)

                                finish()
                            }
                        }

                    } else if(obj.getString("status") == "failed") {
                        this.contentView?.snackbar("Unable to retrieve attendance data")
                        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                        progressBar.visibility = View.INVISIBLE
                    }
                } catch(e:Exception) {
                    longToast("erroexecption"+e.message.toString())
                    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    progressBar.visibility = View.INVISIBLE
                }

                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                // progressBar.visibility = View.INVISIBLE
            },
            Response.ErrorListener {  longToast("errorvolley" +it.message.toString())
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                progressBar.visibility = View.INVISIBLE }){
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("uuid", uniqueID.toString())
                params.put("email", txtEmail.text.toString())
                params.put("events", kodeevent)
                return params
            }
        }

        stringRequest.retryPolicy = DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        queue.add(stringRequest)
    }

    private fun geteventhasuser(){
        val queue = Volley.newRequestQueue(this)
        val url = "http://jimmy.jitusolution.com/api/geteventhasuser"
        // Request a string response from the provided URL.
        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            Response.Listener<String> { response ->
                try {

                   // Log.d("eventhasuser",response.toString())
                    val obj = JSONObject(response.toString())
                    if(obj.getString("status") == "success") {
                        // get events list and save to database

                        val arr = JSONArray(obj.getString("data"))



                        var o = arr.getJSONArray(0)
                        var im:ArrayList<EventHasUserModel> = ArrayList(o.length())


                        Log.d("eventhasuser",  "o length = " + o.length().toString())
       //                 var os = o.getJSONObject(0)

                        for(i in 0 until o.length()) {
                            var os = o.getJSONObject(i)
                            Log.d("eventhasuser",  os.getString("user_username").toString())

                            var og = false
                            if(os.getString("open_gate") == "1") {
                                og = true
                                Log.d("cekboolean", "enterhere")
                            }

                            Log.d("cekopengate", os.getString("user_username") + " " + os.getString("open_gate") + " "+ og)
                            //Log.d("cekboolean", "" + os.getString("user_username") + " " + os.getString("open_gate"))

                           var ehum = EventHasUserModel(0,
                                os.getInt("event_id"),
                                os.getString("user_username"), os.getString("roles"),
                                os.optBoolean("is_deleted", false), og,
                                os.getString("password"), os.getString("email"),
                                os.getString("salt"), os.optString("last_sync", ""),
                               os.optString("inisial", ""))
                            im.add(ehum)
                        }


                       // var cekHasil:ArrayList<EventHasUserModel> = ArrayList()

                        doAsync {
                            try {
                                var x = 1
                                val db =
                                    Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
                                var eventHasUserDao = db.eventHasUserDao()
                                var attendancesDao = db.attendancesDao()
                                attendancesDao.deleteAll()

                                /// jangan lupa ambil data attendanes

                                eventHasUserDao.deleteAll()

                                for (i in 0 until im.size) {
                                    eventHasUserDao.insert(im[i])
                                    Log.d("cekeventuser", im[i].user_username + " " + im[i].event_id)
                                }

                                //cekHasil = eventHasUserDao.getAll() as ArrayList<EventHasUserModel>
                            } catch(e:Exception) {
                                longToast("error" +e.message.toString())
                            }
                            uiThread {
                                // longToast(x.toString())

                                getattendances(im)
                                /*window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                                progressBar.visibility = View.INVISIBLE

                              // Log.d("eventhasuser","cekhasil" +cekHasil.size.toString())

                                val intent = Intent(it, EventActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                                startActivity(intent)

                                finish()*/
                            }
                        }

                    } else if(obj.getString("status") == "failed") {
                        this.contentView?.snackbar("Unable to retrieve event data")
                        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                        progressBar.visibility = View.INVISIBLE
                    }
                } catch(e:Exception) {
                    longToast("erroexecption"+e.message.toString())
                    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    progressBar.visibility = View.INVISIBLE
                }

                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                // progressBar.visibility = View.INVISIBLE
            },
            Response.ErrorListener {  longToast("errorvolley" +it.message.toString())
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                progressBar.visibility = View.INVISIBLE }){
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("uuid", uniqueID.toString())
                params.put("email", txtEmail.text.toString())
                return params
            }
        }

        stringRequest.retryPolicy = DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        queue.add(stringRequest)
    }

    private fun getinvitee(){
        val queue = Volley.newRequestQueue(this)
        val url = "http://jimmy.jitusolution.com/api/getinvitee"
        // Request a string response from the provided URL.
        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            Response.Listener<String> { response ->
                try {
                    val obj = JSONObject(response.toString())
                   // Log.d("inviteecek","response")
                    if(obj.getString("status") == "success") {
                        // get events list and save to database

                        val arr = JSONArray(obj.getString("data"))
                        var im:ArrayList<InviteeModel> = ArrayList(arr.length())
                        for(i in 0 until arr.length()) {
                            var objx = arr.getJSONObject(i)


                            var o = InviteeModel(objx.getInt("id"),
                                objx.getString("name"),
                                objx.getString("code"), objx.getString("address"),
                                objx.getString("email"), objx.getString("description"),
                                objx.optInt("is_attending", 0), objx.getString("message"),
                                objx.optInt("num_of_persons", 0),objx.getString("phone"),
                                objx.getString("side"), objx.getString("status"),
                                objx.getString("tag"), objx.getString("title"), objx.getInt("event_id"),
                                objx.getString("namatable"),
                                objx.getString("qr_code"))
                            im.add(o)
                        }

                        doAsync {
                            try {
                                var x = 1
                                val db =
                                    Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
                                var inviteeDao = db.inviteeDao()

                                inviteeDao.deleteAll()

                                for (objim in im) {
                                    inviteeDao.insert(objim)
                                }
                            } catch(e:Exception) {
                                longToast("error" +e.message.toString())
                            }
                            uiThread {

                                Log.d("startinvitee","oke")
                                // longToast(x.toString())
                                /*window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                                progressBar.visibility = View.INVISIBLE

                                val intent = Intent(it, EventActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                                startActivity(intent)

                                finish()*/
                                geteventhasuser()
                            }
                        }

                    } else if(obj.getString("status") == "failed") {
                        this.contentView?.snackbar("Unable to retrieve event data")
                        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                        progressBar.visibility = View.INVISIBLE
                    }
                } catch(e:Exception) {
                    longToast("erroexecption"+e.message.toString())
                    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    progressBar.visibility = View.INVISIBLE
                }

                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                // progressBar.visibility = View.INVISIBLE
            },
            Response.ErrorListener {  longToast("errorvolley" +it.message.toString())
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                progressBar.visibility = View.INVISIBLE }){
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("uuid", uniqueID.toString())
                params.put("email", txtEmail.text.toString())
                return params
            }
        }

        stringRequest.retryPolicy = DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        queue.add(stringRequest)
    }

    private fun getevents() {
        val queue = Volley.newRequestQueue(this)
        val url = "http://jimmy.jitusolution.com/api/getevents"
        // Request a string response from the provided URL.
        val stringRequest = object : StringRequest(
            Request.Method.POST, url,
            Response.Listener<String> { response ->
                try {

                   // longToast("Initial" )
                    val obj = JSONObject(response.toString())
                    if(obj.getString("status") == "success") {
                        // get events list and save to database
                       // longToast("get event ok")
                        val arr = JSONArray(obj.getString("data"))
                        var em:ArrayList<EventModel> = ArrayList(arr.length())

                        var objinitial = JSONArray(obj.getString("initial"))

                        Log.d("dataevent", arr.toString())
                        Log.d("dataevent", objinitial.toString())


                        for(i in 0 until arr.length()) {
                            var objx = arr.getJSONObject(i)
                            var oi = ""
                            if(objinitial.length() > 0 ) {
                                oi = objinitial.getString(i)
                            }
                            var is_angpau = false
                            var is_note = false
                            var is_person = false
                            if(objx.getString("is_angpau") == "1") {
                                is_angpau = true
                            }
                            if(objx.getString("is_note") == "1") {
                                is_note = true
                            }
                            if(objx.getString("is_person") == "1") {
                                is_person = true
                            }
                            var o = EventModel(objx.getInt("id"),
                                objx.getString("nama"), objx.getString("tanggal_mulai"),
                                objx.getString("tanggal_selesai"), objx.getString("deskripsi"),
                                objx.getString("roles"), oi,is_note, is_angpau,
                                is_person
                               )

                            Log.d("is_angpau", objinitial.toString())
                            em.add(o)
                        }

                        doAsync {

                            var x = 1
                            try {
                                //rest call
                                val db =
                                    Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
                                var eventDao = db.eventDao()

                                eventDao.deleteAll()

                                for (objem in em) {
                                    x++
                                    eventDao.insert(objem)
                                }
                            }
                            catch(e: Exception){
                                Log.d("exerror", e.message.toString())
                            }

                            uiThread {
                                getinvitee()
                                /*window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                                progressBar.visibility = View.INVISIBLE

                                val intent = Intent(it, EventActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                                startActivity(intent)*/

                            }
                        }

                    } else if(obj.getString("status") == "failed") {
                        this.contentView?.snackbar("Unable to retrieve event data")
                        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                        progressBar.visibility = View.INVISIBLE
                    }
                } catch(e:Exception) {
                    longToast("Exception event" + e.message.toString())
                    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    progressBar.visibility = View.INVISIBLE
                }

                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                // progressBar.visibility = View.INVISIBLE
            },
            Response.ErrorListener {  longToast(it.message.toString())
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                progressBar.visibility = View.INVISIBLE }){
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("uuid", uniqueID.toString())
                params.put("email", txtEmail.text.toString())


                Log.d("uuid", uniqueID.toString())
                Log.d("email", txtEmail.text.toString())
                return params
            }
        }

      //  stringRequest.retryPolicy = DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        queue.add(stringRequest)
    }

    override fun onStart() {
        super.onStart()

        // cek
        val sharedPrefs = applicationContext.getSharedPreferences(
            "CURRENT_EVENT_ID", Context.MODE_PRIVATE
        )

        val sharedPrefs2 = applicationContext.getSharedPreferences(
            "USERNAME", Context.MODE_PRIVATE
        )
        var username = sharedPrefs2.getString("USERNAME", null)
        var cureventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)

      //  longToast(username + " " + cureventid)
     //   longToast(username + "  " + cureventid)
       if(username != null && cureventid != null){
            var cek:ArrayList<EventModel> = ArrayList()
            /// cek db
            doAsync {
                val db =
                    Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
                var eventDao = db.eventDao()

                cek = eventDao.getId(cureventid.toInt()) as ArrayList<EventModel>

                uiThread {
                    if(cek.size > 0) {
                        if(cureventid == "0"){
                            val intent = Intent(applicationContext, EventActivity::class.java)
                            // intent.putExtra("eventid", eventList[p0].id)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            applicationContext.startActivity(intent)
                        } else {
                            val intent = Intent(applicationContext, HomeActivity::class.java)
                            intent.putExtra("eventid", cureventid)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            applicationContext.startActivity(intent)
                        }
                    }
                }
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        supportActionBar?.hide()

        uniqueID = id(applicationContext)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)


        // cek
        val sharedPrefs = applicationContext.getSharedPreferences(
            "CURRENT_EVENT_ID", Context.MODE_PRIVATE
        )

        val sharedPrefs2 = applicationContext.getSharedPreferences(
            "USERNAME", Context.MODE_PRIVATE
        )
        var username = sharedPrefs2.getString("USERNAME", null)
        var cureventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)
      //  longToast(username + "  " + cureventid)
        if(cureventid != null){
            var cek:ArrayList<EventModel> = ArrayList()
            /// cek db
            doAsync {
                val db =
                    Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
                var eventDao = db.eventDao()

                cek = eventDao.getId(cureventid.toInt()) as ArrayList<EventModel>

                uiThread {
                    if(cek.size > 0) {
                        if(cureventid == "0"){
                            val intent = Intent(applicationContext, EventActivity::class.java)
                            // intent.putExtra("eventid", eventList[p0].id)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            applicationContext.startActivity(intent)
                        } else {
                            val intent = Intent(applicationContext, HomeActivity::class.java)
                            intent.putExtra("eventid", cureventid)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            applicationContext.startActivity(intent)
                        }
                    }
                }
            }

        }

        /*val editor = sharedPrefs.edit()
        editor.putString("CURRENT_EVENT_ID", cureventid)
        editor.commit()*/



        btnSignIn.setOnClickListener {
            if(Utility.isNetworkConnected(applicationContext)) {
                var validasi = true

                if(!Utility.isValidEmail(txtEmail.text.toString())) {
                    validasi= false
                    txtEmail.error = resources.getString(R.string.invalidemail)
                }

                if(validasi) {
                    // call API sign in
                    window.setFlags(
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    progressBar.visibility = View.VISIBLE

                    val queue = Volley.newRequestQueue(this)
                    val url = "http://jimmy.jitusolution.com/api/signin"
                    // Request a string response from the provided URL.
                    val stringRequest = object : StringRequest(
                        Request.Method.POST, url,
                        Response.Listener<String> { response ->
                          //  longToast(response.toString())
                            try {
                                val obj = JSONObject(response.toString())
                                if(obj.getString("status") == "success") {

                                    val sharedPrefs = applicationContext.getSharedPreferences(
                                        "USERNAME", Context.MODE_PRIVATE
                                    )
                                    var username = sharedPrefs.getString("USERNAME", null)

                                    username = obj.getString("username")
                                    val editor = sharedPrefs.edit()
                                    editor.putString("USERNAME", username)
                                    editor.commit()

                                    getevents()
                                } else if(obj.getString("status") == "failed") {
                                    this.contentView?.snackbar("Incorrect sign in authentication")
                                    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                                    progressBar.visibility = View.INVISIBLE
                                } else {
                                    this.contentView?.snackbar("Something wrong happened")
                                    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                                    progressBar.visibility = View.INVISIBLE
                                }
                            } catch(e:Exception) {
                                longToast(e.message.toString())
                                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                                progressBar.visibility = View.INVISIBLE
                            }


                        },
                        Response.ErrorListener {  longToast(it.message.toString())  }){
                        @Throws(AuthFailureError::class)
                        override fun getParams(): Map<String, String> {
                            val params = HashMap<String, String>()

                            // var haxEmail = Hasher(txtEmail.text.toString())
                            // var hax = Hasher("bOxhTaTyvC22aatPINyx" + haxEmail.hash())

                            // params.put("apikey", hax.hash())
                            params.put("uuid", uniqueID.toString())
                            params.put("email", txtEmail.text.toString())
                            params.put("password", txtPass.text.toString())
                            return params
                        }
                    }

                    stringRequest.retryPolicy = DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
                    queue.add(stringRequest)
                }
            } else {
                contentView?.snackbar(R.string.offline)
            }
        }


       /*try {
            doAsync {
                var x = 1
                val db = Room.databaseBuilder(applicationContext, AppDatabase::class.java, "jimdbv1")
                    .fallbackToDestructiveMigration()
                    .build()
              /*  var eventDao = db.eventDao()

                var em = EventModel(1, "tes", "mulai", "akhir", "deskc", "roles")
                eventDao.insert(em)

                var lst:List<EventModel> = eventDao.getAll()
                Log.d("teslist", lst.size.toString())*/
                uiThread {
                    longToast("migration success")
                }
            }

        } catch(ex:Exception) {
            Log.d("exerror", ex.message.toString() )
        }*/
    }
}
