package com.pentamoo.andre.ksoapp

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView

class QRActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {

    var scannerQR:ZXingScannerView ?= null
    val RECORD_REQUEST_CODE = 101
    var cameraid = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        cameraid = intent.getIntExtra("cameraid", 0)
        val permission = ContextCompat.checkSelfPermission(this,
            Manifest.permission.CAMERA)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i("errorpermission", "Permission to record denied")
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.CAMERA),
                RECORD_REQUEST_CODE)

            scannerQR = ZXingScannerView(this)

            setContentView(scannerQR)
        } else {
            scannerQR = ZXingScannerView(this)
            setContentView(scannerQR)
        }
    }

    override fun handleResult(rawResult: Result) {
       /* Log.v("TAG", rawResult.text) // Prints scan results
        Log.v("TAG", rawResult.barcodeFormat.toString())
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Scan Result")
        builder.setMessage(rawResult.text)
        val alert1 = builder.create()
        alert1.show()

        scannerQR!!.resumeCameraPreview(this)*/
        if(cameraid ==0) {
            val intent = Intent(this, ManualActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
            intent.putExtra("hasilqr", rawResult.text.toString())
            startActivity(intent)
        }else{
            val intent = Intent(this, QuickScanResultActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
            intent.putExtra("hasilqr", rawResult.text.toString())
            startActivity(intent)
        }
        finish()
    }

    override fun onResume() {
        super.onResume()
        scannerQR!!.setResultHandler(this)
        scannerQR!!.startCamera(cameraid)
    }

    override fun onPause() {
        super.onPause()
        scannerQR!!.stopCamera()
    }
}
