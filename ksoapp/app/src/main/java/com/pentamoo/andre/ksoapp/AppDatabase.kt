package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

//This annotation to tell room what is the entity/table of the database
@Database( entities = arrayOf(EventModel::class, InviteeModel::class, AttendancesModel::class, EventHasUserModel::class), version =3, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun eventDao():EventDao
    abstract fun inviteeDao():InviteeDao
    abstract fun attendancesDao():AttendancesDao
    abstract  fun eventHasUserDao():EventHasUserDao
}