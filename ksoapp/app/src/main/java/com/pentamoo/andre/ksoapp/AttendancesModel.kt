package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "attendances")
data class AttendancesModel(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    @ColumnInfo(name = "created_at") var created_at: String?,
    @ColumnInfo(name = "code") var code: String?,
    @ColumnInfo(name = "date") var date: String?,
    @ColumnInfo(name = "deleted_at") var deleted_at: String?,
    @ColumnInfo(name = "updated_at") var updated_at: String?,
    @ColumnInfo(name = "description") var description: String?,
    @ColumnInfo(name = "tipe_scan") var tipe_scan: String?,
    @ColumnInfo(name = "photo_link") var photo_link: String?,
    @ColumnInfo(name = "total_pax") var total_pax: Int?,
    @ColumnInfo(name = "total_angpau") var total_angpau: Int?,
    @ColumnInfo(name = "user_username") var user_username: String?,
    @ColumnInfo(name = "invitee_id") var invitee_id: Int?,
    @ColumnInfo(name = "invitee_event_id") var invitee_event_id: Int,
    @ColumnInfo(name = "need_sync") var need_sync: Boolean



    )