package com.pentamoo.andre.ksoapp

import android.content.Context
import android.support.v4.content.res.ResourcesCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import java.util.zip.Inflater

class EventCardAdapter(ct:Context, user:ArrayList<EventHasUserModel>, totalattendance:ArrayList<Int>): ArrayAdapter<EventHasUserModel>(ct, android.R.layout.simple_list_item_1, user) {
    var ct = ct
    var user = user
    var totalattendance = totalattendance

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var inflater:LayoutInflater = ct.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var v = inflater.inflate(R.layout.card_event_manager, parent, false)

        var pic = v.findViewById<TextView>(R.id.txtPicName)
        pic.text = "Receptionist " + user[position].inisial + " (" + user[position].email + ") "

        var gate = v.findViewById<TextView>(R.id.txtGate)
        Log.d("cekopengate", user[position].open_gate.toString())
        if(user[position].open_gate == true) {
            gate.text = "GATE IS OPEN"
        } else {
            gate.text = "GATE IS CLOSED"
        }

        var tot = v.findViewById<TextView>(R.id.txtTotalScanned)
        tot.text = totalattendance[position].toString() + " scanned"

        var last = v.findViewById<TextView>(R.id.txtLastSync)
        if(user[position].last_sync == "null") {
            last.text = "not synched yet"
        } else {
            last.text = user[position].last_sync

        }

        return v
    }
}