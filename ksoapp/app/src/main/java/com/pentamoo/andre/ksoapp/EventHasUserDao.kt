package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.*

@Dao
interface EventHasUserDao {
    @Query("SELECT * from event_has_user")
    fun getAll(): List<EventHasUserModel>

    @Query("SELECT * from event_has_user WHERE event_id =:eventId")
    fun getId(eventId:Int): List<EventHasUserModel>


    @Query("SELECT * from event_has_user WHERE event_id =:eventId AND roles =:roles")
    fun getIdWithRoles(eventId:Int, roles: String): List<EventHasUserModel>


    @Query("SELECT * from event_has_user WHERE user_username =:username AND event_id =:eventId")
    fun getMyEventHasUser( username:String, eventId: Int): List<EventHasUserModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ehum: EventHasUserModel)

    @Query("SELECT * from event_has_user WHERE email =:email AND roles='event_manager'")
    fun getMyEventHasUserByEventManager( email:String): List<EventHasUserModel>

    @Query("UPDATE event_has_user SET last_sync =:ls WHERE user_username =:username AND event_id =:eventId")
    fun updateLastSync(ls:String, username:String, eventId:Int)

    @Query("UPDATE event_has_user SET open_gate =:og WHERE user_username =:username AND event_id =:eventId")
    fun updateGate(og:Boolean, username:String, eventId:Int)

    @Delete
    fun delete(ehum: EventHasUserModel)

    @Query("DELETE FROM event_has_user")
    fun deleteAll()


}