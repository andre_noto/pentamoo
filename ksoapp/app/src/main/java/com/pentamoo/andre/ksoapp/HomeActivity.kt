package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.menu.MenuView
import android.util.Log
import android.view.Menu
import kotlinx.android.synthetic.main.activity_home.*
import org.jetbrains.anko.*
import org.jetbrains.anko.design.bottomNavigationView
import org.jetbrains.anko.design.snackbar

class HomeActivity : AppCompatActivity() {
    var eventid:String = ""
    var df = DashboardFragment.newInstance()
    var inf = InviteeFragment.newInstance()
    var opf = OptionsFragment.newInstance()
    var lastnaviid = 0

    companion object {
        var user_roles = ""
        var initial = ""
        var email = ""
        var currentpage = R.id.navigation_dashboard
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        var a = menu?.findItem(R.id.navigation_qr)
        if(user_roles == "registrator") {
            a?.setVisible(false)
        } else {
            a?.setVisible(false)
        }

        //longToast("this")
        return true
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_dashboard -> {
                //message.setText(R.string.title_home)

                //DashboardFragment.context = applicationContext

                /*df = DashboardFragment.newInstance()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container, df)
                    .commit()*/

                if(lastnaviid != R.id.navigation_dashboard) {
                    lastnaviid = R.id.navigation_dashboard
                    supportFragmentManager
                        .beginTransaction()
                        .hide(inf)
                        .hide(opf)
                        .show(df)
                        .commit()
                }

                df.populateRecent()
                HomeActivity.currentpage = R.id.navigation_dashboard
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_invitee_list -> {
               // message.setText(R.string.title_dashboard)
                //var ifrag = InviteeFragment.newInstance(

                if(lastnaviid != R.id.navigation_invitee_list) {
                    lastnaviid = R.id.navigation_invitee_list
                    supportFragmentManager
                        .beginTransaction()
                        .hide(df)
                        .hide(opf)
                        .show(inf)
                        .commit()
                }


                HomeActivity.currentpage = R.id.navigation_invitee_list
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_qr -> {
                //message.setText(R.string.title_notifications)

                HomeActivity.currentpage = R.id.navigation_dashboard
                if(user_roles == "registrator" || user_roles == "event_manager") {
                    contentView?.snackbar("This feature is not available")
                } else {

                    if (OptionsFragment.gateOpen) {
                        val intent = Intent(this, QRActivity::class.java)
                        // intent.putExtra("eventid", eventList[p0].id)
                        //intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                        startActivity(intent)
                    } else {
                        toast("Gate is closed. QR Scan is not available")
                    }
                }
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_options -> {
                //message.setText(R.string.title_notifications)

                HomeActivity.currentpage = R.id.navigation_options
                if(lastnaviid != R.id.navigation_options) {
                    lastnaviid = R.id.navigation_options
                    supportFragmentManager
                        .beginTransaction()
                        .hide(df)
                        .hide(inf)
                        .show(opf)
                        .commit()
                }
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onResume() {
        super.onResume()
      /*  supportFragmentManager
            .beginTransaction()
            .hide(inf)
            .hide(opf)
            .show(df)
            .commit()*/

       // onPrepareOptionsMenu(bottomNavigationView().menu)

       /* val sharedPrefs = applicationContext.getSharedPreferences(
            "CURRENT_EVENT_ID", Context.MODE_PRIVATE
        )
        var cureventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)
        toast(eventid.toInt().toString())
        DashboardFragment.context = applicationContext

        df = DashboardFragment.newInstance()
        InviteeFragment.eventid = cureventid.toInt()
        inf = InviteeFragment.newInstance()
        opf = OptionsFragment.newInstance()

        supportFragmentManager
            .beginTransaction()
            .add(R.id.fragment_container, df, "dashboard")
            .add(R.id.fragment_container, inf, "invitee")
            .add(R.id.fragment_container, opf, "options")
            .hide(inf)
            .hide(opf)
            .commit(()*/

        navigation.selectedItemId = HomeActivity.currentpage

    }

    override fun onBackPressed() {
        //super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setSupportActionBar(toolbar2)

        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#555555")))

        val sharedPrefs = applicationContext.getSharedPreferences(
            "CURRENT_EVENT_ID", Context.MODE_PRIVATE
        )

        val sharedPrefs2 = applicationContext.getSharedPreferences(
            "USERNAME", Context.MODE_PRIVATE
        )

        var navi = intent.getIntExtra("navigate", 0)
        if(navi != 0) {
            navigation.selectedItemId = navi
        }
        eventid = sharedPrefs.getString("CURRENT_EVENT_ID", null)
        var username = sharedPrefs2.getString("USERNAME", null)
        Log.d("usernaempass", "cek " + username +" " + eventid )
        lastnaviid = R.id.navigation_dashboard
       // toast("event id " + eventid)
        DashboardFragment.context = applicationContext

        df = DashboardFragment.newInstance()
        InviteeFragment.eventid = eventid.toInt()
        inf = InviteeFragment.newInstance()
        opf = OptionsFragment.newInstance()

        supportFragmentManager
            .beginTransaction()
            .add(R.id.fragment_container, df, "dashboard")
            //.commit()
            .add(R.id.fragment_container, inf, "invitee")
            .add(R.id.fragment_container, opf, "options")
            .hide(inf)
            .hide(opf)
            .commit()

       if(eventid != "0") {
            var u = ArrayList<EventHasUserModel>()
            doAsync {
                var x = 1
                val db =
                    Room.databaseBuilder(applicationContext, AppDatabase::class.java, Utility.dbname).build()
                var eventDao = db.eventDao()
                var user = db.eventHasUserDao()

                u = user.getMyEventHasUser(username.toString(), eventid.toInt()) as ArrayList<EventHasUserModel>
                var eventlist:ArrayList<EventModel> = eventDao.getId(eventid.toInt()) as ArrayList<EventModel>


                uiThread {
                    Log.d("cekusername", "cek " + username +  " " + eventid)
                    supportActionBar?.title=eventlist[0].nama.toString()
                    //longToast()
                    user_roles = u[0].roles.toString()
                    if(u[0].open_gate == true) {
                        OptionsFragment.gateOpen = true
                    } else{
                        OptionsFragment.gateOpen = false
                    }
                    HomeActivity.email = u[0].email.toString()
                    HomeActivity.initial = u[0].inisial.toString()
                    df.populateRecent()
                    opf.refreshFragment()

                    if(user_roles == "registrator") {
                  //      var a = bottomNavigationView().menu.findItem(R.id.navigation_qr)
                    }else {
                        //var a = bottomNavigationView().menu.findItem(R.id.navigation_qr)
                    }
                }
            }
        }
        //supportActionBar?.title="Choose Event"

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }
}
