package com.pentamoo.andre.ksoapp

import android.content.Context
import android.net.ConnectivityManager
import android.text.TextUtils
import android.util.Patterns
import java.security.MessageDigest

class Utility {
    companion object{
        var inviteeNext:Int = 0
        var dbname:String = "jimdb17"
        var CURRENT_EVENT_ID:Int = 0

        fun sha512(input: String) = hashString("SHA-512", input)

        fun sha256(input: String) = hashString("SHA-256", input)

        fun sha1(input: String) = hashString("SHA-1", input)

        /**
         * Supported algorithms on Android:
         *
         * Algorithm	Supported API Levels
         * MD5          1+
         * SHA-1	    1+
         * SHA-224	    1-8,22+
         * SHA-256	    1+
         * SHA-384	    1+
         * SHA-512	    1+
         */
         fun hashString(type: String, input: String): String {
            val HEX_CHARS = "0123456789abcdef"
            val bytes = MessageDigest
                .getInstance(type)
                .digest(input.toByteArray())
            val result = StringBuilder(bytes.size * 2)

            bytes.forEach {
                val i = it.toInt()
                result.append(HEX_CHARS[i shr 4 and 0x0f])
                result.append(HEX_CHARS[i and 0x0f])
            }

            return result.toString()
        }

        fun isNetworkConnected(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
            val networkInfo = connectivityManager.activeNetworkInfo //2
            return networkInfo != null && networkInfo.isConnected //3
        }

        fun isValidEmail(target:String): Boolean {
            return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches())
        }

        fun isAlphaNumeric(target:String): Boolean {
            return target.matches(Regex("[a-zA-Z0-9]+"))
        }

        fun isAlphaNumericSpaceNewline(target:String):Boolean {
            return target.matches(Regex("[a-zA-Z0-9,:!?.;/\\-\\+ \\s]+"))
        }

        fun isAlphaNumericSpace(target:String):Boolean {
            return target.matches(Regex("[a-zA-Z ]+"))
        }

        fun isPassword(target:String):Boolean {
            return target.matches(Regex("^[a-zA-Z0-9]{6,10}\$"))
        }

        fun convertDate(tanggal:String):String {
            var thn = tanggal.substring(0,4)
            var bln = tanggal.substring(5,7)
            var tgl = tanggal.substring(8,10)

            when (bln) {
                "01" -> return(tgl + " Januari " + thn)
                "02" -> return(tgl + " Februari " + thn)
                "03" -> return(tgl + " Maret " + thn)
                "04" -> return(tgl + " April " + thn)
                "05" -> return(tgl + " Mei " + thn)
                "06" -> return(tgl + " Juni " + thn)
                "07" ->return(tgl + " Juli " + thn)
                "08" -> return(tgl + " Agustus " + thn)
                "09" -> return(tgl + " September " + thn)
                "10" -> return(tgl + " Oktober " + thn)
                "11" -> return(tgl + " November " + thn)
                "12" -> return(tgl + " Desember " + thn)
            }

            return ""
        }
    }
}