package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.*

@Dao
interface AttendancesDao {
    @Query("SELECT * from attendances")
    fun getAll(): List<AttendancesModel>


    @Query("SELECT * from attendances WHERE invitee_id =:invId")
    fun getId(invId:Int): List<AttendancesModel>

    @Query("SELECT * from attendances WHERE invitee_event_id =:evtId")
    fun getAllSpecificEvent(evtId:Int): List<AttendancesModel>

    @Query("SELECT * from attendances WHERE invitee_event_id =:evtId AND user_username =:username")
    fun getAllByUsername(evtId:Int, username: String): List<AttendancesModel>

    @Query("SELECT * from attendances WHERE invitee_event_id =:evtId AND user_username =:username")
    fun getAllRecent(evtId:Int, username: String): List<AttendancesModel>

    @Query("SELECT * from attendances WHERE invitee_event_id =:evtId AND user_username =:username")
    fun getTotalAttend(evtId:Int, username:String): List<AttendancesModel>



    @Query("UPDATE attendances SET total_pax =:totpax, total_angpau =:totangpau, description=:desc WHERE invitee_id =:invid")
    fun updateId(totpax:Int, totangpau:Int, desc:String, invid: Int)

    @Query("UPDATE attendances SET total_pax =:totpax WHERE invitee_id =:invid")
    fun updateTotPax(totpax:Int, invid: Int)

    @Query("UPDATE attendances SET need_sync =:ns WHERE invitee_id =:invid")
    fun updateNeedSync(ns:Boolean, invid: Int)

    @Query("UPDATE attendances SET photo_link =:photo WHERE invitee_id =:invid")
    fun updatePhoto(photo:String, invid: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(am: AttendancesModel)

    @Delete
    fun delete(am: AttendancesModel)

    @Query("DELETE FROM attendances")
    fun deleteAll()
}