package com.pentamoo.andre.ksoapp

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "event_has_user")
class EventHasUserModel(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    @ColumnInfo(name = "event_id") var event_id: Int?,
    @ColumnInfo(name = "user_username") var user_username: String?,
    @ColumnInfo(name = "roles") var roles: String?,
    @ColumnInfo(name = "is_deleted") var is_deleted: Boolean?,
    @ColumnInfo(name = "open_gate") var open_gate: Boolean?,
    @ColumnInfo(name = "password") var password: String?,
    @ColumnInfo(name = "email") var email: String?,
    @ColumnInfo(name = "salt") var salt: String?,
    @ColumnInfo(name = "last_sync") var last_sync: String?,
    @ColumnInfo(name = "inisial") var inisial:String?
    )